「呼……這樣子就搬完家了呢♪」


數日後——

在新屋子的臥室裡，艾麗婭用手腕擦了擦額頭上的汗水。


完成租賃合同的簽署後，艾麗婭她們便馬上購買了之後需要的傢俱。
然後，包括床在內的所有生活需要的用具，都在今日送達並完成了布置。


「啊哈哈！比起旅店的床還要軟綿綿的哇！」
「可以蹦蹦跳跳，好好玩~！」


莉莉和菲莉在臥室裡的雙人床上又跑又跳地嬉戲打鬧。
艾麗婭購買了兩張雙人床，材質上挑選了稍微有些高等的那種，而莉莉和菲莉二人對此都感到非常滿足。


「呼啊……塔瑪的抱抱，實屬久違了……甚是舒心！」


在另外一張床上，史黛拉帶著一臉恍惚的表情將塔瑪抱在自己的懷裡。
在購買雙人床時，史黛拉使用了龍人化，僅靠一人之力從都市的傢俱店裡將兩張床運送到新家裡去。
作為獎勵——不如說是作為辛勤勞動的回報，艾麗婭將塔瑪的擁抱權給予了史黛拉。


——喂史黛拉，不要一直摸吾輩的頭吶。
——不！可抱塔瑪之事實為難得！直至盡興為止吾都要一摸到底！


因為被史黛拉一直不停地摸頭，塔瑪想讓她停下來而送去了念話，結果反而讓史黛拉的摸頭攻勢更上一層樓。
不僅如此，史黛拉抱著塔瑪的力度也變得更大，將塔瑪的臉「呣扭！」地埋在自己的酥胸裡。


(唔，沒想到會有被史黛拉做這種事的一天到來啊……人生真是什麼事都有可能發生吶)


曾幾何時，塔瑪和史黛拉還是互相廝殺的關係。
當時無論怎麼想，都預料不到會變成現在這樣的吧。


(呣，塔瑪又在和史黛拉醬無言對視了。到底是在做什麼呢……？)


看著氣氛變得越來越好的一人跟一貓，艾麗婭的雙頰稍稍鼓起地吃著醋。


「不好意思打擾了~！我是送包裹的！」
「啊，好——的！現在就過去！」

就在這時，玄關處響起了一道呼喊聲。
艾麗婭一邊回應，一邊下到一樓去開門迎接客人。
而現身在門前的，是商業區服裝店的老闆娘。

「特意給我們送過來真是感激不盡！」
「沒什麼，為了艾麗婭醬的話這不算什麼事！因為艾麗婭醬是這個都市……不，是這個國家的英雄啊！再加上這一次的訂單還挺有趣的呢」
「英雄什麼的……不過能被這麼說我很高興呢♪」
「太謙虛了啦，果然艾麗婭醬好可愛呢。那麼我就先回店裡去了，如果有什麼不合身的地方請儘管提出來，這邊會馬上調整的喲」
「好的，十分感謝」


艾麗婭從老闆娘的手上接過箱子，目送她離去後，便帶著一臉笑容回到了二樓。


「莉莉醬、菲莉醬，之前委託定做的衣物到了哦！」
「真的！？看一下看一下！」
「哇~！好期待啊~！」

聽到這話後，莉莉和菲莉二人的眼睛閃閃發光地跑了過去。
艾麗婭將箱子放在她們面前，拿出了箱子裡的東西。


「誒~！比想像中的還要漂亮呢！」
「觸感也好順滑，穿著好舒服~！」
「呵呵，兩人都好可愛啊♡」


箱子裡的東西是莉莉和菲莉的衣物。

艾麗婭對莉莉和菲莉一直都是全裸狀態感到很在意，於是便在定下要住在這裡的那天，向服裝店定做了兩人要穿的衣物。而為她們二人量身定做的特製衣物終於在今天送來了。
剛才老闆娘說的有趣的訂單就是指她們二人的衣物訂單。

莉莉的是淡粉色的連衣裙。
菲莉的則是淡綠色的。

兩件衣服都非常合身，正如艾麗婭所言，兩人都顯得十分可愛動人。
順帶一提，莉莉的那件衣服為了讓她的翅膀能舒展開來，特意做成了後背大開的類型。
和衣服一樣，衣服底下的內衣也是定製的。
由於二人都在高興地轉圈圈，所以和衣服一樣顏色的胖次有點若隱若現。


這對妖精狂熱者和蘿莉控來說，是會讓他們把持不住的光景吧。
不過明明至今為止都是堂堂正正地全裸著的，為什麼現在這種稍微走光的情景反而顯得更為下流呢。


「啊對了！難得兩人都穿得這麼可愛，塔瑪也來打扮一下吧！」
「喵、喵(不、不會吧吾主喲)！？」


對艾麗婭的想法，塔瑪不由得貓軀一震。


「唔，怎麼了塔瑪。在害怕著何物？」


史黛拉發現塔瑪在她懷裡突然繃緊了身體，於是疑惑地問道。


如果塔瑪沒有猜錯的話，這個時候就應該立馬逃跑的。
但如果這麼做的話，艾麗婭肯定會變得悲傷起來。
塔瑪在「要保住男人的尊嚴」和「作為騎士不能讓主人變得悲傷」這兩個感情之間搖擺不定。


「嗯唔……應該是在這裡的——啊有了！」

在新買的壁櫥裡，艾麗婭翻出了一樣東西。


(啊啊……果然是這樣嗎……)


看到那東西，塔瑪如同放棄般地垂下了頭。


艾麗婭拿出來的東西，是她定做的塔瑪專用的小裙子。
那是一條以淡藍色和純白色作為主色調，邊緣用荷葉邊裝飾著的可愛的母貓用小裙子……。
高傲的騎士要是穿上了話，那實在是太過屈辱了。


然而——

「呀~！塔瑪你怎麼能這麼可愛啊！」
「簡直就是貓貓公主~！」


結果，由於情感上輸給了「不想看到艾麗婭悲傷的表情」那邊，塔瑪只好被逼穿上了這條小裙子。
而那實在是太過可愛了，莉莉和菲莉都興奮地大喊。


「什麼！？此是何等的惹人憐愛！為何，塔瑪的此等面貌，讓吾下腹處變得難耐起來了！」


看到塔瑪的小裙子裝扮，史黛拉在別的意義上絕贊大興奮中。
她用兩手按住下腹，大腿也忸忸怩怩地互相摩擦著。


「哈啊……塔瑪真是的，為什麼可以這麼可愛呢……♡」


艾麗婭和史黛拉一樣，臉上布滿紅暈，輕咬著食指露出一副開心又困擾的表情。
而她那美麗的冰藍色瞳孔中的粉色小心心，暴露了她也正在興奮中的事實……。

(還、還好現在不是和吾主獨處，真是太好了……)


要是一個不好可能就會發展成塔瑪被艾麗婭襲擊的狀況，想像到這事的塔瑪鬆了一口氣。


「嗯喵~，大家都在喵？」


正當美少女四人都沉溺在塔瑪的美貌中不可自拔時，一樓的玄關處傳來的這樣的聲音。


「薇兒卡桑！怎麼了嗎？拿著好多東西呢……」


這次來的客人是薇兒卡，不知道為何她的手上和背上都有著好多東西在。


「今天是艾麗婭醬你們遷居的紀念日喵，機會喵得，我今天店裡就早早打烊，來給你們慶祝了喵！」


一邊說著，薇兒卡一邊將行李都放到餐桌上並將裡面的東西擺放出來。

「哇！是荷包蝦！這邊是霜降牛肉！還有那是……！」


薇兒卡帶過來的是好幾種高級食材。
看來為了慶祝艾麗婭她們的遷居，她做了各種各樣的準備的樣子。


「肉！有散發著美味氣息的肉！」


史黛拉聞到了上等肉的香氣，噠噠噠地從二樓飛奔了下來。


「甜甜的香氣！這是蛋糕的氣味呢！」
「食慾都被勾起來了~！」

莉莉和菲莉則是被甜品的香氣給勾引出來了。
那香氣的源頭是薇兒卡從迷宮都市有名的點心店裡買來的大蛋糕。


「居然帶來了這麼多東西……真是太感謝你了，薇兒卡桑！」
「要給夥伴慶祝的話，這種程度是當然的喵♪艾麗婭醬你們搬家已經很累喵吧，好好休息一下喵！」
「不不，我也要幫忙！別看我這樣，還是挺擅長料理的哦？」
「那麼就一起來做飯吧喵！」


於是艾麗婭和薇兒卡便有說有笑地開始了食材的料理，史黛拉她們三人則在一旁滿臉期待地看著。
兩個親密無間的大姐姐，和一臉期待地等著料理的三個小孩子……看到這樣的光景，塔瑪在心裡苦笑了一下。




「好了，這樣就完成了喵！」
「終於等到了！」
「哇~看上去好好吃~！」
「何等的香氣！快開吃吧！」


莉莉和菲莉，還有史黛拉三人已經垂涎三尺，看上去已經再也等不了的樣子了。
做出來的菜有烤牛排、烤荷包蝦，還有沙拉、湯和甜點。

艾麗婭和薇兒卡喝的飲料是好幾種類型的酒。
史黛拉、莉莉和菲莉的是果汁。
而給塔瑪準備的則是高級牛奶。

「那麼，為了慶祝遷居……乾杯喵！」
「「「乾杯！」」」

在薇兒卡的帶頭下，眾人紛紛舉起了杯子。


史黛拉、莉莉和菲莉都像小孩子一樣大口大口地喝著果汁，十分天真可愛。
在一旁的艾麗婭和薇兒卡則是小抿一口度數稍微偏高的蜂蜜酒，「呼~」地發出了有點色氣的吐息聲。
而塔瑪在艾麗婭的座位前小口小口地舔著牛奶。


「……！此、此肉，美味至極啊！」

一口氣喝乾果汁的史黛拉立馬就吃上了牛排，隨即便被嘴裡擴散開來的美味感動得睜大了眼睛。

「這麼說的話，史黛拉還是第一次嘗到霜降肉的料理呢，很好吃對吧？」
「艾麗婭，此物名為霜降肉嗎……吾記住了！」


兩面都烤得正好的霜降牛肉味道本就非常醇厚，在其上簡單地撒上少量鹽和胡椒調味後，便足以讓人回味無窮。


「來，塔瑪，啊~嗯♡」
「喵啊~！」

塔瑪也從艾麗婭處得到了牛排並一口咬上去。
瞬間，牛排的肉汁在口中迸發蔓延，讓人感到滿滿的幸福感。


「好吃！我第一次吃到蝦這種東西呢，好美味哇！」
「蘸著黃油醬吃真是絕配呢~！」


莉莉和菲莉的舌頭已經被這個都市的特產——烤荷包蝦給俘虜了。
她們二人一直都生活在森林型的迷宮裡，海產的滋味還是首次嘗到吧。


荷包蝦在蝦類裡也是以口感特別爽脆、味道特別濃郁聞名的高級食材。
第一次吃海鮮便能吃到荷包蝦的話，會被其味道征服也是理所當然的了。


「喵~！艾麗婭醬果真很擅長料理啊喵！湯的味道也超贊的喵！」
「呵呵，能合你胃口真是太好了♪」

薇兒卡對艾麗婭做的海鮮湯贊口不絕。
其他海鮮料理的味道都很濃郁，但唯獨這湯的口感有點清淡。而正是海鮮湯的這份清淡，絕妙地讓其他海鮮的味道充分地展露了出來。

(呼呣，這口感和公會、旅館的料理不相上下——不，是那之上的美味啊！)


塔瑪也從盛了一些湯的小碟子中試著舔了幾口，便在心裡對艾麗婭高超的料理技藝感到佩服。


在享受著美食和甜點的同時，五人和一貓興緻逐漸變得高漲了起來。之後，大家都度過了一段愉快的時光。
