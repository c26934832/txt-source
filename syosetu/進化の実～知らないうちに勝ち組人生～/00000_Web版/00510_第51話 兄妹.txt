﻿
在少女吃完豬排蓋飯後，我決定把一開始露易絲所委託的事情完成。

『奴役的項圈』擁有一種要是有人嘗試強行移除，佩帶者就會遭受異常的疼痛，甚至因此而死的惡毒效果。
正因如此，世界各國大都廣泛的禁止使用，但看起來凱澤魯帝國明顯沒有這樣設下定制。
不管怎麼說，當務之急是幫眼前的少女去除項圈。
為了讓她能稍微安心下來，我蹲到了（少女）眼睛的高度，並盡可能的用友好的語調說話。

「從現在開始，我想試著把施加在你脖子上的項圈取下來。」
「嗚！？......這種事情，怎麼可能做到。」
「嗯，我覺得你的反應是正確的。畢竟，非施加者的人要除去『奴役的項圈』被說了是不可能的嘛。」
「那麼――。」
「沒問題的！就交給歐尼醬我吧！」

為了給予進入放棄狀態的少女，即使只有一點點的希望也好，信心滿滿的回答了。
常有人說過於龐大的力量會招致自身的毀滅，所以我，對於自己那怪物般的狀態值（status）是感到害怕的。
但是，如果我的力量能對他人有所説明的話，無論如何我都會放棄謹慎（動用力量）。
有著能拯救他人的力量這件事，我希望即使到了那時候（自滅）都能對此感到自豪。
我對附加在少女身上的項圈，抬起了手。
恐怕，對上這個項圈，我的原創魔法『好起來』不會產生任何效果。
因為『好起來』的效果，是用魔法將『詛咒』轉變成『祝福』嘛。
此外，能將『奴役的項圈』解除的，就只有施加者本人而已。
專門除去（項圈）的魔法什麼的，也不存在類似這樣的東西。
因此，我非得創造出新的魔法不可。
......不會再發生跟『好起來』同樣的失敗了喔！？
沒騙人喔！？
話是這麼說，要從『奴役的項圈』中解放的話該用什麼形象才好啊？
想在這裡，我停了下來。
......阿～～勒？
比『好起來』那時候還更難想像啊？
還，還沒結束！
要放棄還太早了！
對，對了，照著奴隸，奴役，解放之類的單詞，想像之後浮現出來的那些東西就可以了！
呃......奴，奴隸？
奴役？
......無法想像啊啊啊啊啊啊啊啊啊！
不對，並不是真的完全沒有，不是還有一個的嘛？
但是啊？
我那與奴隸解放有關的知識啊――

「可是林○總統啊......！」
『......』

搞砸了啊啊啊啊啊啊啊啊啊啊啊啊啊！
又幹了一次蠢事了啊！看吧！看看大家的臉！『咦......○肯總統？那是什麼？』都是這副表情啊！
我現在超想馬上跑掉。
然而，緊接著，我舉起的手發出了光芒，光芒飛向了少女的項圈，當光芒接觸到項圈的瞬間，項圈噴裂四散了。

「嗚喔！？好危險！」

我，避開了以猛烈氣勢飛散的碎片。
順帶一提，露易絲跟為了能處理好技能而不斷訓練的我，某種程度上能憑藉自己的意志活動身體，雖然還不完整，但這樣一來多少能減少被技能控制的情況。
少女的話，很幸運的沒有受傷，看起來可能會被碎片擊中的人只有我。
......為啥啊？
總而言之，少女一時之間沒有發現到項圈被取下的事實，最後輕輕地摸了摸自己的脖子，確定沒有了項圈。

「嗚！？」
「你看！做到了！」

到最後，雖然全力的搞砸了，但完成了解放所以算是通通解決了吧！
會這麼說的原因，說出來很浪費心中的自豪感，是因為在腦中一直聽到聲音。

『技能『魔法創造』已經發動。已創造解放魔法『林○總統』』

對不起，果然還是完全沒解決！
雖然在那之後我認為自己有在堤防，但還是搞砸地創造了怪名字的魔法不是嗎！？
首先，明明是魔法名，卻有著人的名字耶！？
不，因為這件事情我也很想哭啊！
在心中仍然驕傲，同時腦中苦悶的情況下，新想像出來的魔法顯示出了說明。

『解放魔法：林○總統』――從各種拘束以及阻礙中解放的魔法。

即使如此還是毫無例外的出現了荒謬絕綸的效果！
這個，不只是奴役，好像對各種類型的束縛系魔法都能發揮效果的樣子耶！？
慘了......真不愧是自由之國的總統。
......是誰說了句不錯的話啊？是我呢。
正當獨自一人悲哀的進行搞笑吐槽時，露易絲的聲音從後面傳了過來。

「師傅！這樣是......成功了，可以這麼想嗎？」
「啊啊，大概吧！」

項圈被取下後，我再次使用技能『世界眼』進行確認，『狀態：隷役』的項目已經被漂亮的清除掉了。

「......怎麼會......真的......。」
「嗯？怎麼了嗎？」

沒想到，我注意到少女的様子有點奇怪，這麼出聲回應之後，下一秒，少女開始大哭了起來。

「嗚......嗚......咕嘶......。」
「嚇！？等......為啥哭了起來啊！？」
「呀――咿呀――咿！誠一先生把女孩子弄哭啦！」
「羅娜小姐，你給我乖乖閉嘴！」

即使對發出奇怪煽動的羅娜小姐吐了槽，我還是不知道該如何是好，只能完全的處於狼狽狀態。
然後，至今為止都默默看著事情發展的莎莉婭走了過去，擁抱住少女。

「乖，乖！沒問題的呦～～因為太高興了，所以才哭出來的對吧～～？」
「......咕嘶......嗚嗯！」
「也是呢......雖然不知道發生了什麼事，但這樣的少女一直是『奴役的項圈』這種邪道才會使用的道具的受害者。在那之後得到解放，不可能不開心的吧！」

注意到時阿爾也，向著少女靠近，撫摸著她的頭。

「嚼嚼，雖然搞不太清楚是怎麼回事，不過真是太好了呢，嚼嚼！」
「你什麼時候把豬排蓋飯給！？」

露露妮嘛，還是老樣子。
過了一段時間，被莎莉婭與阿爾兩人安撫的少女，終於停止了哭泣並開口說話。

「......嗯，謝謝你！」
「別介意喲！」

看著擦拭眼睛，乖巧說出感謝之言的少女，莎莉婭露出了溫柔的微笑。

「......我的名字是，奧爾嘉・卡爾梅利納。」
「是叫奧爾嘉醬嗎？我是莎莉婭！請多指教囉！」

該怎麼說才好呢。
莎莉婭，很容易解開女孩子的心防啊！

「......嗯，莎莉婭姊姊！」
「姊姊？我嗎？哎嘿嘿，總覺得有點難為情呢。」

竟然還被叫成姊姊！？
對於莎莉婭所擁有的，天生的母性，我只能感到戰慄。
緊接著，少女
――奧爾嘉醬將視線移到了阿爾那邊。

「......姐姐的名字是什麼呢？」
「啊？俺嗎？俺叫阿爾托莉亞喔！」
「......阿爾托莉亞姐姐？」
「――。」

被稱呼為姊姊的阿爾，看起來好像很錯愕的樣子，現在很明顯的開始慌亂了。

「怎，怎怎怎麼辦啊，誠一？總覺得，被奧爾嘉這孩子叫了姐姐之後......從胸口裡面，好像......感受到了某種溫馨事物啊！？」
「那個，不就是開心的感覺嗎？」

為啥你要這樣不停的原地打轉啊？
我冷靜的進行吐槽之後，不知為何露露妮以莫名其妙的驕傲狀態站了出來。

「叫奧爾嘉的，我是被當作主人騎士的僕人露露妮喔！」
「......饕餮？」
「為什麼啊！？明明都叫了那兩個人姊姊，為什麼不這樣叫我啊！？」
「我想是平時行為的問題。話說，就這麼想被叫姐姐嗎......？」

在進行這樣的對話時，奧爾嘉醬已經離開了莎莉婭的身邊，朝著露易絲那裏移動。

「......姐姐！」
「嗯？怎麼了嗎？」
「......名字！」
「啊啊，說起來還沒自我介紹呢。我是露易絲。請多多指教呢，奧爾嘉！」
「......嗯，露易絲姊姊！」
「嗚！？」

奧爾嘉的話語，讓露易絲睜大了眼睛，看來是受到不小的衝擊呢。

「......姊姊，嗎......對沒有弟妹的我來說，聽起來......很不錯呢。」
「哈哈哈。確實是這樣呢，畢竟露易絲只跟我成了兄妹嘛。」

露易絲也，正因為是妹妹，似乎有著想被叫姊姊的願望，（對姊姊的稱呼）產生了超出預期的反應。
我因為（露易絲的）那個模樣而露出苦笑時，我的長袍被奧爾嘉醬拉扯著。

「......名字。」
「我？我叫誠一，請多指教啦，奧爾嘉醬！」
「......嗯，誠一歐尼醬。」

這樣說著，奧爾嘉醬微微的笑了。
......該如何形容呢，這種感覺。
雖然從小就被作為翔太妹妹的美羽用誠一歐尼醬這麼叫著，但被奧爾嘉醬這麼稱呼時，會有種難以言喻的衝動在體內流竄呐。
我也，沒辦法對阿爾的事情說什麼大話呢......
正當我這麼想的時候，羅娜小姐興奮得（向奧爾嘉醬）問道。

「奧，奧爾嘉醬！我是羅娜喲！」
「......。」
然後，奧爾嘉醬躲到了我的身後，小聲說道。

「......討厭這個人！」
「為什麼呀呀呀呀呀呀呀呀呀呀呀呀！！？？」
「......因為是壞心眼。」

顯然奧爾嘉醬對羅娜小姐的第一印象真的糟糕到了極點。

◇◇◇◆◆◆◇◇◇

在所有人都自我介紹完畢後，再次開始了對奧爾嘉醬的詢問。
但是，因為羅娜小姐進行尋問時，（奧爾嘉）什麼話也不說的緣故，無奈之下只好改由露易絲來負責。
此外，我們也獲准能直接參與奧爾嘉醬她們的詢問。
......雖然羅娜小姐看上去像是個優秀的審訊官，我還是有點懷疑這是不是在騙人啊。

「所以呢......奧爾嘉，你是，為了什麼才去襲擊陛下的？」
「......我，其實是不想襲擊的。但是，身體卻沒辦法這麼做......。」
「果然啊......。」

奧爾嘉醬的回答，讓露易絲皺起了眉毛。
雖然有預測到，果然奧爾嘉醬並不是自己喜歡才去襲擊藍澤先生的啊。
說來說去，一切都是由於『隷役項圈』的命令。
命令，基本上是無法違背的。
要說理由，是因為（項圈）會直接以身體為對象來執行命令。
儘管如此，要是違反命令的話，強烈的疼痛就會襲擊奧爾嘉醬的身體。
想到了這一點，我也不由自主的皺起了眉頭，此時奧爾嘉醬朝我這裡發出了傷心的視線。

「......誠一歐尼醬，對不起！雖然是接收到『抹殺目擊者』的命令，但攻擊了誠一歐尼醬依然是事實......。」
「我才是，抱歉呐！」
「哎？」

奧爾嘉醬，像是沒想到會從我這得到道歉一樣，瞪大了眼睛。

「我啊，並不是對奧爾嘉醬輕輕的拍了下什麼的，而是很強勁的衝撃喲？沒有大礙吧？」
「......嗯，沒問題！疼痛什麼的，很久以前就習慣了......。」

這麼說著的奧爾嘉醬，表情變得更加得難過，悲傷了。
在我們因為那種表情而不由得摒住呼吸的時候，奧爾嘉醬開始小聲說道。

「......我，正如你們所見是貓的獸人，但......是個忌子......。」
「......。」
「......對貓的獸人而言，黑是不幸與災厄的象徴。所以，一直被媽媽打著，『不需要的孩子啊』這樣，被說了很多次......。」
「......。」
「......可是，我期待著啊！想被撫摸呀！想被用笑容面對呀！想要被喜愛呀......！......這些事情已經，無法成真了。我，被做為奴隸賣給了凱澤魯帝國......。」
「......。」

對於從奧爾嘉醬口中說出的，大量的衝撃性發言，我除了默默聽著以外無法有別的動作。
被孕育出自己的父母，當成不需要的孩子來對待，無論如何都是件冷酷無情的事啊。
我的家人，相處得就很不錯。
雖然被當成是理所當然的事情，但並不是這樣的。
不管我怎麼改變，都能不曾改變地給予愛的父母，真的是非常的，勝過一切的祝福著我。

「......然後我，很快就被買走了。被買下後，先進入了王族直屬的暗殺部隊，在那裡，我――將殺人的方法融入了自身......。」
「......。」

那麼小的孩子，居然將殺人的技術......

「......因為我（年紀）很小，並沒有讓我去做取悅男人的事情。但是，做為交換，把大量的人殺掉了。因為只有這件事，是我存在的價値，所以，我已經......是汙穢的人了。即使不被命令指使殺害國王，我的手......也早就是汙穢的了......。」

這麼說著，奧爾嘉醬低下了頭。
......這個孩子，除了殺人以外沒有能看見自己價值的方法。
這是因為，從小時候就被親人以暴力相待，沒有感受過像樣的親情的關係吧。
這麼思考著的我，沒有多想身體就自然而然的動作了。
把像是會被折斷一般嬌嫩的奧爾嘉醬從正面擁抱著。

「沒問題的！奧爾嘉醬不是汙穢的喔！因為，真正汙穢的人......是不會做出哭泣這種事的吧。」

奧爾嘉醬她，哭了。
是跟先前不同的，悲傷的眼淚。
幸福的眼淚，想流多少都行。
但是......悲傷的眼涙什麼的，是誰都不想看見的吧。
緩慢的輕撫著（奧爾嘉醬的）頭與後背的同時，我說道。

「你看，想被撫摸的話，不管幾次我都會撫摸著！想要被用笑容面對的話，包括奧爾嘉醬在內，不管何時都會露出笑容，這個......來看看吧！」
「？」

我把頭罩脫下，顯露出自己的髪色。

「......啊......！？」
「哪？我也跟奧爾嘉醬一樣，頭髪，是黑色的喔？另外，眼瞳啥的也是黑，這樣的話，看上去就像真正的兄妹也說不定喔？雖然臉長得不一樣啦......。」
「兄......妹......？」
「是呀，是呀！而且，奧爾嘉醬啊，之後你打算怎麼辦呢？」
「......沒有，什麼要去做的事......因為，我已經沒有棲身之所了......。」
「那麼，跟我們在一起不就好了嗎？」
「哎！？」

我的提案，讓奧爾嘉醬用吃驚的表情抬頭看著我。
但是，很快的把臉朝下了，搖起了頭。

「......不行！要是，我跟誠一歐尼醬在一起的話，誠一歐尼醬也會被變成凱澤魯帝國針對的目標。背叛者，不管是因為什麼理由，都是不可原諒的！」
「這樣啊，那麼，我會使盡全力守護住的！」
「！？」

奧爾嘉醬再次抬起了頭，看著我的臉。
......老實說，心情非常不好。
再說，提到凱澤魯帝國，似乎就是對我的朋友們......
翔太他們用了勇者召喚的國家。
對於那個國家，能平靜使用『奴役項圈』的狀況，我感到很焦躁。
翔太他們平安無事吧？
雖然我記得，有聽到（他們）已經開始去上學了......
包含那件事，如果我的朋友們出了什麼事的話，我
――是絕對不會饒恕（帝國）的。
即使，真的會被稱作怪物，我也有著舍去一切，橫衝直撞的信心。
......但現在，我能做的事，就是把眼前的奧爾嘉醬，從凱澤魯帝國的手中保護好。

「......真的，可以嗎......？」
「嗯！」
「......還能，像剛才那樣被撫摸嗎......？」
「不管幾次都撫摸給你看。即使毛髪亂糟糟的，我也不會在意喔！」
「......會對我，露出笑容嘛？」
「這種事不是當然的嗎？話說，我基本上是個不安分的傢夥呀，要有所覚悟喔？要笑的可不只是我，奧爾嘉醬也得露出笑容喔？」
「......嗯......嗯！」

奧爾嘉醬，因為我說的話，再一次淚如雨下了。
但是，這並不是悲傷的涙水，而是幸福的眼涙。
所以，大哭也是可以的。
再次把奧爾嘉醬的身體抱進懷裡，往後背輕輕地，溫柔地拍著的我，把視線移到了露易絲那邊。

「......就是這麼一回事啦......露易絲，可不可以把奧爾嘉醬交給我呢？」

雖然對話的進展不錯，但畢竟奧爾嘉醬是襲擊了藍澤先生的人物，說不定還是個知道凱澤魯帝國情報的重要存在。
所以說，一下子就交給我這種事才不會發――

「可以啊！因為是師傅嘛。」
「很可怕耶！？最近露易絲都只用『師傅』這個詞就把話題結束了啊！」

這麼簡單就獲准了。
咦？好奇怪啊？！

「一開始，我就有託付給師傅的打算了。」
「是這樣嗎？」
「不論是我，還是陛下，對奧爾嘉都是持保護的動作。對如此年幼的孩子，做不出懲罰之類的事情啊。因此，雖然我提出了保護奧爾嘉的意見，但我作為要保護陛下的騎士，顯然無法做到對奧爾嘉的照顧。交給哥哥是不錯，哥哥卻也是要務纏身的樣子......。」
「嗯――的確很忙那！」
「所以說，我想可以的話就交給師傅了。如果是師傅的話，就算用凱澤魯帝國派來的刺客來假設，也有著絕對性的安全。」
「居然說是絕對安全......你到底把我看成什麼人啊？」
「？是師傅吧？」
「會問你是我腦殘！？」

『師傅』這個單詞，越來越向著萬能化發展了。

「哈......算了！總之，從今以後請多關照啦，奧爾嘉醬！」
「......嗯，請多指教，誠一歐尼醬！」
「耶――！奧爾嘉醬，跟我們在一起了呢！」

然後，莎莉婭朝奧爾嘉醬飛撲了過去，擁抱在一起。

「......？莎莉婭姊姊也？」
「不只是莎莉婭而已喔。俺也會一起相處喔！」
「哼，畢竟我是主人的騎士嘛，就讓你跟著一起行動吧！」
「......阿爾托莉亞姐姐跟貪吃鬼......。」
「喂！差不多別再貪吃鬼，貪吃鬼的叫我了吧！」
「不，根本名副其實吧！」
「連主人也！？」

知道被我當成貪吃鬼的露露妮，很明顯的消沉了。

「......是這樣啊，我，不過就是個貪吃鬼啊。是嗎，是嗎......」 （癱坐）
「不用那麼失落吧......不管怎麼說，我還蠻喜歡露露妮吃得津津有味的模樣耶。」
「真，真的嗎？主人......。」
「嗯，嗯，所以打起精神來吧！」
「......是！我，從今以後會更加努力，去考慮吃飯的事情的！」
「努力的方向好奇怪啊！」

什麼嘛？
到了這個地步露露妮依舊是本性難移啊。
不，這樣應該是好事吧。
剛才的氛圍就像是謊言一般，輕鬆的氣氛在我們之間流動著。
――只有一個人例外。

「請，請讓我也加入吧！」
「......不要！討厭這個人！」
「為什麼啦――！？」

羅娜小姐，似乎真的被奧爾嘉醬厭惡著呀。
......話是這麼說，但（奧爾嘉醬）那因為羅娜小姐垂頭喪氣的樣子，而浮現出來的小小微笑，我想其實不是真心討厭她的吧。

「......食物的怨恨是，很可怕的喔。」
「......。」

訂正！
果然是非常討厭也說不定呢。我對羅娜小姐合了下掌。

◇◇◇◆◆◆◇◇◇

「話說回來，露易絲最後是不是想拜託我什麼啊？」

把奧爾嘉醬的事大致處理好的時候，我這麼問了露易絲。

「是這樣沒錯！這件想要拜託的事情，就是希望師傅能在我離開的期間，擔任陛下的護衛工作。」
「誒？讓我來做藍澤先生的護衛？倒不如說，你是要去哪裡啊？」
「是的！其實，同期之中雖然有個被稱做『黑之聖騎士』的人，但跟我不同，主要是在威恩布魯古王國內進行巡邏，並解決各式各樣的問題。然後，就在最近，跟那個『黑之聖騎士』取得了聯繫，在國境附近接到了大量有關魔物行動持續活性化的報告。」
「原來如此......所以說，露易絲也要作為協助者，被叫過去幫忙嗎？」
「正如您所說！『黑之聖騎士』雖然被稱作是守護的天才，但那個人也只有一個身體。因為完全集中在一個地方的話，就沒有辦法前往其他地點的緣故，那邊就得由我率領著『聖劍的女武神』前往。」
「原來是這麼回事啊......可是啊，如果是這種重要事情的話，不要拜託我，去找那些在城堡中服務，更加優秀的士兵不是更好嗎？」
「不，因為不管怎麼做，只要一和師傅相比就無法有安全感......另外，雖然說是護衛，並不代表得一直陪在陛下的身邊不可。」
「是這樣嗎？」

露易絲出乎意料的言語，讓我有點驚訝。
我還以為，肯定要像字面上一樣每時每刻都處在一塊呢。

「我所希望的是，我不在的期間，師傅不要從這個特魯貝魯轉移出去，這麼一回事。只要師傅還在這個城市裡，那我也能更加的放心了。」
「雖然你給了我非常高的評價啦，但我只不過是個普通的冒險者耶......。」
「師傅！普通的冒險者，想要把我......『劍騎士』打倒是不可能的事情喔。」
「是，對不起！」

慘了！我的感覺，好像跟周遭的感覺之間產生了巨大的偏差啊。

「嘛，隨便了！如果是這件事的話，就交給我吧！話是這麼說，真的只要做到不從特魯貝魯轉移這種事就行了嗎......。」
「這樣就可以了！非常感謝您，師傅！」
「別在意啦！然後呢？大概要多久才能回來呢？」
「雖然不打算浪費那麼多時間，但我想即使只是移動也要花上至少一周的時間。」
「明白了......把它當成一個月的長度來考慮會比較好吧。」
「再一次地對此謝罪，對不起，師傅！」
「我的事情怎樣都好。露易絲你也一樣，要注意安全啊！」
「好的！」

露易絲她，帶著雖然微小，但能清楚看見的微笑，回應了我的話語。
過了不久，露易絲帶著『聖劍的女武神』的所有人，出發了。
但是，什麼事都沒發生就結束了，『世界才不是這麼運作的啊』，這樣，不禁深深的體會到了。
那是，在從露易絲他們出發算起，一個星期後發生的的事。

