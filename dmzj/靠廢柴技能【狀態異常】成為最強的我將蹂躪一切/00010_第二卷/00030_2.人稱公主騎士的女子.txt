兩人組感到疑惑。

「具體上要怎麼做？要我們殺了她嗎？」
「沒錯，殺了她。」
「喔。」

兩人組的反應難以捉摸。
但是，孟克告訴他們報酬的金額後，兩人組眼神立刻一變。

「你的委託，我們接了。」
「但是，如果那女的向你們求饒，你們判斷她是打從心底為她對我的態度感到後悔，就別當場殺了她。」
「殺她之前，我們可以『享樂』吧？」
「當然。如果你們活捉到她，就隨便你們蹂躪吧！」
「喔，我可以大幹一場對吧？」
「當然！她貶低了我的自尊！她兩度踐踏了我的自尊！老子我一定要讓她在這裡負起兩人份的責任！隨你們開心，盡情干壊她！」

禿頭用袖子抹掉流下的口水。

「嘿嘿，我全身上下的幹勁都涌上來了⋯⋯那傢伙可是個上等的貨色。」
「就算她穿著衣服我也看得出來，那身材真讓人受不了。我無論如何一定要活捉她。啊，我好想看⋯⋯我好想看那個女人臉孔扭曲、哭泣大叫的表情啊啊啊♪」

禿頭提出疑問。

「但是，那女的好歹也是征戰各地的戰士吧？她的武藝如何？」
「看起來應該還不差。但是那女的已經累到不行了。仔細一看，可以看見她眼睛下面有黑眼圈。剛才，我在上方階層，注意到她走路偶爾還搖搖晃晃的。只不過──雖然我毫無疑問可以打贏她──考慮到凡事都有萬一，我才來找你們。因為我想確實地幹掉她。」

禿頭像在誇示力量一樣，舉起斧頭用力一揮。

「不管怎樣，反正區區一個女人不可能打贏我們！畢竟對方又不是女神薇希斯！」

看來，在不同的通道裡，有時使用斧頭會比揮劍容易。
在這座遺跡裡，劍刃太長有時反而會變成致命傷。
但是──沒想到居然會在這裡聽見混帳女神的名字。看來她應該是相當知名的神吧。
孟克堅信不移地說：

「那女人⋯⋯看起來明明累到不行，卻拚命往前趕路。她的目的一定是龍眼聖杯吧！那女的好像很想要錢。」
「她一定是個見錢眼開的拜金女。為了錢什麼都肯做。」
「你們覺得這麼做怎樣？我們先找到龍眼聖杯，假裝要讓給她，然後趁她不注意，打倒她呢？」
「喔，不錯耶！你真是天才！」
「如果她求饒的話──就直接把她留在身邊當奴隷吧！」
「最後，再把她賣去妓院！就算品質下降，以她那姿色，絶對可以賣個好價錢！」

兩人組聊得興高采烈。此時，孟克心有不服地插嘴。

「喂、喂喂！第一要件是確實殺了那女的，好嗎？你們聽懂了沒？」
「嘿嘿，看來你真的很不爽那女的耶？」
「問什麼蠢問題！我真的很火大⋯⋯一想到那女的把我當笨蛋耍，還毫不在乎地繼續呼吸、活在這個世界上⋯⋯而且，一想到她以後還會忘了我，若無其事地繼續生活！我就氣到一肚子火，想狠狠地撕裂胸口啊啊啊啊啊啊！」

孟克吐出扭曲的憤怒。

「如果沒看到她那張漂亮臉蛋絶望而死的表情──我就無法安眠！她侮辱了我兩次！竟然有種侮辱老子我？而且還是兩次！我一定要殺了她⋯⋯把她那漂亮的臉蛋和身體拿去喂魔物⋯⋯」

兩人組看起來顯得有些反感。

「讓那女人變成那種下場，我其實也沒差啦⋯⋯但是至少讓我們兩個侵犯她吧？只殺她，實在太可惜了。」
「那就等你們兩個侵犯她之後，我再把她拿去當魔物的飼料⋯⋯飼料⋯⋯飼料飼料飼料啊啊啊啊啊啊！」

兩人組的笑容逐漸僵硬。就像在說「這傢伙沒救了」的表情。
孟克指著和我所在之處正好相反的通道。

「聽好囉？我們躲在那裡埋伏，等那女的一下樓梯就發動攻擊。那裡有一塊看起來很適合用來當偷襲地點的陰影處。」

孟克剛才說他在上方階層看到了彌絲朵。
這表示，彌絲朵接下來，恐怕就會往下來到這個階層。
默不吭聲觀察著情況的我，從陰影後站出身來。禿頭發現了我。

「啊⋯⋯？你誰啊？剛才的話，你全部都偷聽到了嗎？」
「你們真是一群無藥可救的傢伙。」

孟克以充滿血絲的眼睛狠狠瞪著我。

「啥──你說什麼？你這臭小鬼，對本大爺胡說八道什麼喔喔喔喔喔喔！小心老子殺了你！啊啊，我懂了我懂了！我也順便打碎你身為人類的自尊，拿去喂魔物吧啊啊啊啊！」

這傢伙未免太容易发親了吧？
兩人組也拿起武器。

「嘖！個連毛都還沒長齊的臭小鬼，竟然跳出來想裝好人！我就殺了你，省得你礙事！」
「咯咯咯！反正在遺跡裡發現屍體，根本是家常便飯！喔！我想到一個好主意了！把這傢伙的手腳砍下來，再把他活活拿去喂魔物吧！」

我朝他們伸出手。

「等等。」
「啊啊，怎樣？」
「是我錯了。求你們饒了我吧。」
「噗哈！發現對方比自己還要厲害的瞬間，就馬上跪地求饒？小鬼，你遜斃了！不過呢，已經來不及了──」
「【PARALYZE】。」

──動彈不得──

「怎麼、一回、事──⋯⋯嗯？」
「搞什、麼⋯⋯？不能、動了⋯⋯？」

現在跟他們對峙之後，我才明白。先前的四人組，跟這三個人比起來，實在厲害太多了。
這三個人全身上下漏洞百出。我根本連出其不意發動攻擊的必要都沒有。

「為什、麼⋯⋯？」

孟克睜著眼睛，非常困惑。

「你幹了、什⋯⋯麼？」
「你說呢？比起那個──」

我移動到孟克的正前方，在他耳邊低聲說：

「你剛才說，你要殺了誰？」
「咿──」

孟克發出簡短的哀號。聲音裡透露著膽怯。

「你──你⋯⋯你是、什麼⋯⋯東西？怎麼、突然⋯⋯變得⋯⋯判若兩人⋯⋯」
「呵呵，抱歉啊。」

我差點因為自己這種垃圾行為反胃作嘔。這反應應該算是好事吧？
可能是因為想起他們的關係吧？

「『驅逐』你們這樣的人──不可思議地，讓人心情非常愉快。殺了你們，說不定也沒什麼不好。」
「咦？」

不僅如此。當然也是為了幫彌絲朵一把。
我也是人。這幾個傢伙和彌絲朵，如果問我要幫誰，根本不用多說。
不是做法正確或錯誤的問題。
要站在哪一邊──我可以自己決定。
登河・三森要支持誰的陣線，決定權在我一個人身上。所以，我決定隨心所欲。

「你們剛剛好像說過，遺跡裡出現屍體是家常便飯，對吧？」

複數對象指定。

「【POISON】。」

三人陷入中毒狀態。

「嗚、喔、惡⋯⋯什、麼？怎麼、回、事⋯⋯」「不、不能呼吸了⋯⋯」「救、命──」

孟克充滿恨意地瞪著我。

「你給我、記、住⋯⋯我一定⋯⋯會、讓你、后⋯⋯悔⋯⋯」
「咯咯咯，你是笨蛋吧？正因為如此──」

我露出扭曲的笑容嘲笑他們。

「我就先在這裡徹底擊潰你們吧。」

既然你們想殺我。
我就殺了你們。

「嗚！嗚、嗚、嗚⋯⋯」

我發現了。
好幾只魔物的氣息，正往這裡逐漸靠近
離去之際，我丟下一句話。

「結果──最後變成魔物飼料的人，是你們。」

我彎過通道轉角時，另一頭，魔物的叫聲蜂擁而至。

「嘰嘎啊啊！」「吼嘎嘎嘎嘎嘎！」「咕喔嘎啊啊！」

逐漸逼近的叫聲嘎然停止。接著，叫聲變得更加凶暴。

「嘰嘶啊啊啊啊啊啊──！」

從聲音的位置判斷，正好停在孟克他們那一帶。
身體麻痺又中毒的孟克和兩人組。
他們是因為中毒斷了氣？還是被魔物殺了？我不知道。
我彷佛聽見一陣短促的慘叫。突起狀的嗶嘰丸延伸，探出頭來。

「嗶！」

嗶嘰丸有點憤怒。似乎是在氣剛才那三個人。

「你還真是好惡分明⋯⋯」
「嗶！」

我待在通道轉角打發時間，過了一會兒。

「嗶嗶嗶？嗶嘰！」

嗶嘰丸發出警告。
在孟克他們原本所在之處大吵大鬧的那群魔物，騷動的聲音停止了。
叫聲再度往這邊靠近。大概是循著氣味過來的吧？

「它們的下一個獵物好像是我耶。」
「嘎嘶啊啊啊啊啊啊！」

我朝從轉角露出臉和手的魔物，釋出我最常用的連續攻擊。

「嗚、吼、嘎──⋯⋯？」

成功封殺了襲擊而來的魔物。
魔物似乎並未受到孟克他們身上的毒素影響。
我的狀態異常技能所釋出的毒性，不會影響到對象者之外的人嗎？
我回到孟克他們原本所在的地方，才發現他們早就被吃掉了。
鮮血淋漓的屍體。飛濺四散的血液。我本來想搶走他們身上的錢──後來決定放棄。
小袋子破了，硬幣散落一地。硬幣上也沾到了血。
帶這些錢回地面上時，負責檢查的官員可能會注意到這些硬幣。

「萬一他問我這些錢是怎麼來的，視情況，說明工作可能會變得很複雜⋯⋯」

在地下一枚一枚擦掉硬幣上的血，也很花時間。幸好，我身上帶的錢還很足夠。
因此我決定不碰那些屍體，放在原地，任其自生自滅。
我折返走回通道。那群全身麻痺又陷入中毒狀態的魔物還活著。

⋯⋯等待他們中毒而死也很麻煩。

於是我拿起短劍，照順序一個一個割開魔物的喉嚨。
這樣或多或少可以得到一些經驗值吧？但是，自從我進入這座遺跡之後，等級到現在都還沒提升。我也殺了不少魔物⋯⋯可是經驗值仍然很低。
照這樣看來，好像沒有必要勉強去殺害魔物了。
我判斷已經處理好魔物之後，往下一個階層前進。
這座遺跡每一層的空間，都比我原本想像的狹小──應該說，感覺很狹窄。
有可能只是因為廢棄遺跡太寬闊了。
我用麻痺和毒發動連續攻擊，殺死魔物，繼續往前邁進。

【SLEEP】因為有射程的問題，所以我把它當成重複施展、以策安全的手段。
麻痺的射程大約２０公尺。用起來就像遠距離武器的感覺。
因此第一發攻擊，我無論如何都會使用【PARALYZE】

戰況較為輕鬆時，我會再加上【SLEEP（眠性賦予）】

因為我想快點讓【SLEEP】也到達等級３。
愈往前進，看到其他傭兵的頻率就愈少。
等級依然維持原狀，沒有上升。我躲在陰影處，確認MP。


～～～～～～～～～～～～～～～～～～～～

MP：＋５８５１７／５９０３７


～～～～～～～～～～～～～～～～～～～～

看這MP的剩餘量，好像不需要太擔心。
順便一提，我到今天早上才發現，MP會在睡眠時恢復。
我昨晚睡覺前檢查過MP殘留量。今天早上起床時確認，發現已經全部恢復了。
經過一定時間以上的睡眠，MP就可以恢復。
我想起在廢棄遺跡裡，住在住宿區時發生的事。那時候因等級提升快速，連帶著MP不斷回復，所以我檢查的頻率也跟著降低。但是說不定，睡眠也能使MP完全恢復。
不管怎樣，這下就能稍微輕鬆地使用MP了。
我確認了一下懷錶。這支表是我和彌絲朵在之前採買的店裡所購買的。

「稍微休息一下吧？」
「嗶。」

這座遺跡裡有好幾個休息用的房間。據說是傭兵還有侯爵的私人軍隊，前來探索時所設置。
聽說很難得，但是偶爾會有幾個親切的人，放些可以保存的食物和飮水在這裡。
不過人類一走，有很高的機率會被魔物佔領。
我在第１０階層發現了休息小屋。門上用塗料畫了一個記號。好像就是休息室的標誌。
但是，那間房裡已經有先來的客人了。房間裡傳出聲音。是人類的聲音。裡頭並沒有魔物。我將提防背後攻擊的警戒工作交給嗶嘰丸，豎耳傾聽。

「回過神來，已經到第１０層了！」
「其他那些傭兵，速度比較快的，現在應該還在第７層吧？」
「因為現在的我們，有好好休息過啊。」
「果然，一馬當先衝進來遺跡裡面，才最有利。」
「不過相對之下，我們就得負責收拾掉從新階層跑出來的強大魔物。」

他們難道是⋯⋯說明會結束後，立刻潛入遺跡的傭兵？

「很好啊！龍眼聖杯就由我們劍虎團收下了！」
「好耶！」
「再往下走５層就是新階層了！你們可要好好打起精神來喔！」
「包在我們身上，大姊！」
「很好！回答得很好！」

在這裡的那群傭兵，似乎是最快攻到這裡的一組⋯⋯

這個階層並未看見其他傭兵的身影。
話說回來──我不知道怎麼了。
現在只想一個人休息。待在這個房間，感覺會讓精神更疲憊。

⋯⋯對了。去找看看還有沒有其他空著的休息室，進去裡面休息吧。

於是我邊殺著魔物，邊繼續往下走了三層。
１３層。只要再往下走兩層，就是發現新階層的區域。
我想在踏進新階層之前，好好休息一下。
我四處張望，終於找到先前也看過的休息室標誌。

「咕嘎嚕啊啊！」「咘吼喔！」

那裡已經被魔物佔據了。裡面有十只。魔物的殺意散溢而出。
複數對象指定。

「【PARALYZE】。」
「咕、嘎！嗚、嘎⋯⋯？」
「【POISON】。」

我以技能消滅了佔據房間的魔物。但是──

「得把這些屍體搬出去嗎⋯⋯」

裡面有幾隻身軀龐大的。

⋯⋯先去下面那一層看看吧。

進入１４層，我一樣邊殺著魔物邊進行探索。但是，沒發現休息室。

「還是不行嗎？」

無可奈何，我只好折返，走回１３層的休息室。
接著，我將魔物屍體搬出房間外，再回到清理乾淨的房間。

「呼。」

這房間的構造跟廢棄遺跡的居住區截然不同。

「我睡覺的期間，可以拜託你幫忙把風嗎？」
「嗶！」

有一個可以幫忙把風的伙伴存在，真是太好了。
跟之前在廢棄遺跡時，神經緊繃幾乎無法休息的時候比起來，簡直天差地遠。
我背靠著牆，閉目養神。
我睜開眼睛。意識逐漸覺醒，我確認了一下懷錶。

「────睡了三個小時嗎？」
「嗶嘰。」

毫無異常！──

嗶嘰丸就像在說這句話一樣，伸出突起的觸手向我敬禮。

「你真是幫了大忙。等等，你不睡覺沒關係嗎？」
「嗶！」
「可是，如果你想休息的話，不要客氣，儘管告訴我喔？反正我們沒有必要參加龍眼聖杯的爭奪戰。不需要勉強趕時間攻克這座遺跡。」
「嗶♪」

我從沒看過嗶嘰丸睡覺的模樣。它們沒有所謂睡眠的概念嗎？
之前問它的時候，它也只回我一聲：

『嗶？』

給了我一個不置可否的反應。我不可思議地想著，將肉乾遞給嗶嘰丸。
史萊姆還有很多未解的謎團。

「嗶咻咻咻～♪嗶啾嗶啾啾～♪哞啾哞啾啾～♪」

它很開心地吃著肉乾⋯⋯原來它也跟其他生物一樣有食欲啊。
我們簡單填滿空腹後，走出房間。再次走向通往１４層的樓梯。
因為之前已經下去過一次了，所以我知道樓梯的位置在哪裡。

「嗯？」

一個臉色非常難看的男人朝我接近。背後還有其他傭兵的身影。
我輕輕擺起架式。他們沒有敵意──看起來是如此。

「喂，你。」

這聲音⋯⋯是之前在休息室裡那組最快攻到這裡的隊伍嗎？
我決定改用謙虛的模式應對。

「請問怎麼了嗎？」
「這座遺跡，現在有點奇怪。」
「奇怪？你們發現了什麼嗎？」

劍虎團的成員站在我面前。
所有人的臉色都不怎麼好看。難道發生了什麼異常事態嗎？
紅髮女子不甘心地咒罵了一句。

「可惡──差一點點就到新階層了。」

褐色肌膚的男人朝背後一瞥。

「這座米魯茲遺跡，現在發生了某些前所未有的事！剛才我們遇見從上面來的人，大家都已經感受到了異狀。」

我睡了三個小時左右。那段期間發生了某些事。從他們說話的語氣和表情來看，並不像是騙人的，那是真的碰上預料之外的事時，才會露出的表情。
我擔憂地詢問。

「請問，具體上來說，發生了什麼異常狀況？不好意思，這是在下第一次進入米魯茲遺跡。」

褐色肌膚男子露出玄妙的表情回答。

「魔物都死了，而且死狀怪異。」
「死狀怪異⋯⋯？」
「沒錯。不可思議的是，我們無法找出它們死亡的原因。屍體上沒有傷痕。也沒有什麼特別奇怪的地方。只是皮膚變得毫無生氣而已──」

⋯⋯嗯？

紅髮女子接著男人的話繼續說。

「所有魔物不論強弱，全都死狀怪異。還有啊，我們剛才去了下面的１４層。這次最先進去的應該是我們才對。你知道嗎？連從來沒有其他人進去過的１４層魔物，也全都死光了。」

休息前，我曾經去過第１４層，才回來這裡。
我在１４層的時候，順便把那裡的魔物也殺了。
因此至少有一個人進過那個階層才對⋯⋯

「之前剛從上面下來的那群人也說，到處都可以看到沒有外傷也毫無異狀的魔物屍體。可是，我們下來的路上並沒有看見。」

紅髮女子繼續說。

「簡單來說，也就是短短幾個小時間，整座遺跡開始起了變化。」

其他傭兵一個接一個開口。

「說不定是新階層裡冒出了毒霧。有沒有可能是那些霧冒上來了？」
「到目前為止，還沒有人類遇害⋯⋯只不過，影響可能會過一段時間才出現。那可能是遲效性的毒霧，我們也該將這點列入考慮。」
「還有人在說，會不會是以前擁有龍眼聖杯的國王，所下的詛咒。」
「不管怎樣，在發現新階層之前，並未出現過這樣的現象⋯⋯」

紅髮女子雙手抱在胸前，朝伙伴們一瞥。

「我最重視的，莫過於劍虎團和這群傢伙了⋯⋯我不打算帶他們繼續往這座狀態詭異的遺跡前進。雖然有點遺憾，但是我決定折返。首先⋯⋯先觀察情況再說。」

褐色肌膚男人對紅髮女子點頭，然後對我說：

「如果你遇見其他傭兵，能不能盡可能把這件事情告訴他們？要不要前進就交給他們各自判斷⋯⋯但是，我想他們必須知道發生了這樣的變異才行。」
「好。」
「你可能會覺得我多管閑事，但是我勸你，還是快點回去上面比較好。反正調查的事，就交給哈克雷老頭的私人軍隊就好了。畢竟那老頭，好像真的很想要那個聖杯。」

我謙虛地微笑。

「多謝你的關心。」
「對了？一個人嗎？我看你沒帶什麼裝備耶。」

一個人在這一層，太奇怪了嗎？而且我的裝備只有短劍，和一把小榔頭。

「不是，我有伙伴。」

我強調背後背的袋子。

「我負責將可作為材料的魔物分屍、提行李。只不過，我們似乎在躲避魔物攻擊時，不小心走散了⋯⋯等回過神來，才發現我走到這一層了。」

萬一被問，我就說出彌絲朵的名字，蒙混過去吧。
但是，幸好他們並未追問我伙伴的名字。

「啊，原來是那樣啊。就是在逃命躲避魔物時，不知不覺闖入了超乎自己能力所及的階層是嗎？那你要跟我們一起回去上面嗎？」
「感謝你的提議，各位的好意我心領了。但是我不能丟下伙伴自己走。」

傭兵們面面相覷。好像在用眼神彼此詢問著「怎麼辦？」

「我知道了。不過你可別亂來喔？」
「好。」
「很抱歉，我們現在精神上沒有餘力幫你尋找伙伴。不過──講難聽一點，我們也沒有理由為一個素不相識的人顧慮那麼多就是了。」
「不、不，各位光是願意告訴我這裡發生的變異，就很足夠了。」

劍虎團離開後，我邁向１４層。
嗶嘰丸「嗶？」了一聲，探出頭來。

「啊啊，對了。剛才他們話題裡談到的『變異』，一定是指我用技能殺害的魔物屍體吧。」

因為我實在沒有氣力再去處理屍體和偽造死因。畢竟數量太多了⋯⋯

但是反過來想想，可以說攻略新階層變得輕鬆多了。
只要關於變異的事情繼續往上傳，傭兵的數量應該會減少。如此一來，就能降低捲入和其他傭兵間紛爭的機率。也不用擔心有人看見我使用技能而大驚小怪了。

「對現在的我來說，搞不好更方便⋯⋯」

之後，我很順利地通過１４層。之前已經聽說這一層並沒有什麼陷阱類的東西。
我就這樣，直接前往新發現的第１５層。
前方終於開始出現沒有輝石的通道──可是，並不是什麼大問題。
跟廢棄遺跡相較之下，這裡簡直就像天堂。

「而且我還有這個啊。」

我拿出皮囊，注入魔素。
因為我現在有麻袋和背包，所以皮囊裡面空無一物，提起來輕鬆很多。
到了新階層後，我沒有立刻通往１６層。
我確認地圖。
從１５層開始，空間是往橫向延伸的⋯⋯也就是說──

「找到了隱藏通道。」

我往前進。雖然說輝石數量變少了，但是這一帶的牆壁上還嵌著一些。

「儘管不多，但光是有光源就足夠了⋯⋯」

這裡沒有其他人類的氣息。
我繼續朝深處前進，邊走邊在地圖上填入資訊。
不過，沒有必要填滿。記下我走過的地方就好。製作地圖等有空的時候再慢慢弄。我隨手在地圖上寫下簡單的內容。
我一路上遇見的魔物──寫不了。
這麼說來，出現的全是我不知道正式名稱的魔物。

「⋯⋯關於魔物的記述，還是算了吧。」

書寫內容的期間，由嗶嘰丸負責警戒。

「嗶嘰！」

突起的觸手指向背後。我轉身向後，出現了一群魔物。
是一群有著馬頭人身的魔物。也就是跟半人馬正好相反的類型。

「嘶咿咿咿咿咿咿欸呀呀呀呀啊啊啊啊──！」

我釋出固定的連續攻擊招式。魔物們中了麻痺和毒。
至今還沒有會陷入苦戰的跡象。
我背向魔物屍體，踏出步伐。如果把整個過程當作一個故事來看，現在這個階段，簡直可說是平舖直述，沒有任何高潮迭起。

「哼──正如字面上一樣，沒什麼好說的。」
「嗶！」

我往下走了兩層。
１７層沿路都是非常像遺跡的景色。給人一種地下迷宮遺跡的感覺。
我發現了很像居住區的區域。居住區內的大房間裡，有裝著貴金屬的架子。大概是過去住在遺跡裡的居民，所擁有的物品吧？我只拿了體積較小的貴金屬和寶石。
我在背包裡留了一些空間，用來堆放材料。
我穿越居住區，繼續往前進。
因為是從未有人踏入的地區，所以並沒有傭兵設置的休息室。
算了，萬一有需要，再暫時回去居住區休息。
不過，我並沒有看見只要注入魔素就會自動開關的門扉。也沒辦法上鎖。也就是說，這裡並不是一個完全安全的空間。這麼一來，就算能睡，也只能淺眠了。

我想著這些事情，走著走著──來到一條天花板很高的通道。
我往路的盡頭走近，前方出現了一道氣派的門。

⋯⋯這裡看起來，貌似確實有些什麼東西。

身體的疲勞還不成問題。

「走吧。」

在這裡停滯不前也不是辦法。
我站在門前。雙手用力推門。
中途，一鼓作氣地推開門板。
並立刻往房間外移動，背靠著牆壁。直接走進去暴露自己的行踪這種做法，實在太危險了。
首先該做的是觀察。畢竟我之前也曾因為噬魂魔的雷射，嘗過不少苦頭⋯⋯

我警戒著，探頭窺看房間。
空氣裡飄著灰塵。從建築的構造來看，很像一座神殿。輝石隔著一定的距離，鑲嵌在牆上。黑牆和白光的對比，美不勝收。應該是經由人類雙手布置的吧？
在房間最深處，可以看見一尊人型龍（我只能這麼描述）的石像。
巨大的石像⋯⋯嗯？等一下？人型龍的石像？
有著祭壇跟人型龍石像的房間──

「⋯⋯⋯⋯」

我緊盯著房間深處的祭壇。祭壇上放著杯子。
這些杯子莫名地給人一種深具威嚴的感覺。
可以說，就像坐鎮在此一樣。杯子外面也鑲嵌著輝煌的石子。
類似寶石的石頭。讓人聯想到爬虫類的眼睛。也就是說──

「那就是龍眼聖杯嗎⋯⋯？」

看來我好像抵達侯爵招募傭兵攻略遺跡的目的地了。
我踏進房間，這裡也感受不到魔物的氣息。嗶嘰丸也──沒有反應。
至少現在沒有。
我移動到祭壇前面，抬頭看著牆上的石像⋯⋯突然有種預感。

「這個大概會動吧。」

只要是多少接觸過現代日本故事的人，誰都能料想得到。只要想伸手拿取寶物，就會開始動的機關。RPG裡常見的手法。這尊石像，絶對有問題。

⋯⋯先下手為強好了。

我向石像伸出手。

「【PARALYZE】。」

突然──石像的頭頂到鎖骨帶變色了。嗶嘰丸產生反應。

「嗶？嗶！」

石像布滿鱗片的皮膚，漸漸染上生命的氣息。

「嘰嘎嘎嘎嘎嘎嘎嘎，嘶啊啊啊啊！」

緊接著，冒出明顯的殺意和魔物的氣息──只不過，為時已晚。

「吼──喔嘎？」

魔物陷入麻痺狀態。恐怕是麻痺的影響吧？石像中途就停止變色了。
我接著賦予毒。
巨大的龍人脖子上方，全變成中毒的顏色。

「嘎、嘎、嘎⋯⋯？嘰，嘎，嘎嘎⋯⋯！」

變色中途停止。雖然只有脖子上方，但是石像仍試著掙扎。
石像拚命地想從口中噴出什麼東西，但是⋯⋯

在麻痺狀態下，愈是勉強行動，就愈接近死亡。
噗咻咻咻咻！噗咻咻咻咻！
鮮血從龍人的耳朵和嘴巴噴濺而出。這是想憑蠻力移動時會出現的現象。

「吼、嘎⋯⋯」

傳來骨肉扭轉分離的聲音，肉從鎖骨上方一片一片掉下來。吐出舌頭的頭部，滾落地面。
接著，殘留的石像崩塌碎裂。大小不一的石塊散落在地板上。
就像所謂的寶藏守門人滴水嘴獸雕像一樣。
這就是湊齊條件後，石像就會開始移動的魔物式陷阱。

「說到石像，還是噬魂魔比較厲害，不過幸好沒有那種自動迎擊系統⋯⋯」

那真的很麻煩。
如果噬魂魔沒有凌虐弱者的興趣，我應該打不贏它吧？
我舉起龍眼聖杯。

「所以這個銀杯⋯⋯就是這次活動的『矚目商品』是嗎？」

銀色杯子在不同角度下，會發出紫色的光澤。金色寶石或許是在模擬龍的眼睛。
杯腳部分，讓人聯想到龍的前爪。杯子的容器部分，設計成前爪由下握住容器的樣子。
我以袖子擦拭有點髒的表面後，光澤的表面反射出輝石的光芒。

「好漂亮啊。」

杯子大小正好可以帶得回去。所以，該怎麼辦呢──嗯？

「嗶！」

背後傳來氣息。我轉過身，同時往應該能隱藏住身影的祭壇陰影處移動。
退後的同時──伸出手臂。
但是，我沒有發動狀態異常技能。

「⋯⋯是你。」

一張熟識的面孔就站在門口。或許是她沒機會知道傭兵們之間開始流傳的「變異」吧？不然就是跟我一樣，決定抓住這個大好機會，繼續探索遺跡。
走進房間的人發現是我後，也逐漸放下了警戒心。

「原來是你嗎？」

彌絲朵・巴魯卡斯。

現在的彌絲朵除了衣服之外，身上只穿了防具和護額。
我並未看見盔甲。她沒有帶來嗎？
護額的一部分有光芒蓄積。像水銀一樣濃稠的光芒。
她用那個來代替燈嗎？

彌絲朵護額上的光逐漸變得微弱。現在，地下輝石照出她苗條的身影。不知道該怎麼說，感覺跟這座魔物蠢蠢欲動的地下遺跡格格不入。
我總覺得在皇宮內穿上禮服比較適合她。
彌絲朵懊悔地垂下雙眼。她的右手緊緊抓住左手臂。
就像在壓抑情緒一樣。表情非常黯淡。

「因為其他傭兵都折返了，我還以為我是第一個到達這裡的人。」

她似乎察覺到變異了，但是選擇冒著風險繼續攻略遺跡。
彌絲朵的眉毛變成八字。臉上硬擠出微笑。
她的聲音聽起來很無助。還隱藏著一點斥責之意。

「看來我的想法還是太天真了。我跟一群魔物纏鬥了一陣子之後，決定稍事休息，沒想到拖延到攻略速度，慢了一步。」

她內心隱含的感情，或許是自責吧？就像在責備自己能力不足一樣。嗯──

「你想要的話，這個杯子給你吧？」

彌絲朵發出「咦？」一聲，抬起頭來。表情一臉茫然。

「你剛剛說什麼⋯⋯？」
「我說你想要的話，就給你。」
「你──想要什麼代價？」
「代價？」
「如果你要我付出金幣30o枚作為交換的代價⋯⋯我現在拿得出來的東西──」

彌絲朵的視線有氣無力地看向下半身。她輕輕將手按在腰間的劍柄上。

「可能只有這把劍吧⋯⋯但是，這把劍沒有金幣３００枚的價值。哈提大人，有什麼我可以為你效勞、作為交換條件的事嗎？」

彌絲朵將手按在胸口上。簡直就像立誓效忠的騎士。

「只要是我做得到的事，請你儘管開口。」

我走向彌絲朵面前。

「只要是你做得到的事，什麼都可以嗎？」

彌絲朵咽了一口唾液。她略顯困惑地移開視線。

「總、總之⋯⋯請先告訴我，你的願望吧？我先聽完你的願望後，實際上看看可不可行，再考慮一下──」
「你拿去吧。」

我遞出龍眼聖杯。彌絲朵露出不可思議的表情。

「咦？什麼意思？」
「給你。」

我將聖杯一把推給她之後，立刻放開手。彌絲朵慌張地「啊」了一聲，用雙手抓住聖杯。

「請問⋯⋯哈提大人，你希望我為你實現什麼願望，來交換這個聖杯──」
「我沒有什麼特別想實現的願望。」
「不，這關係到我的尊嚴！不管是要我幫你提行李、張羅飮食、護衛，什麼都可以，請你儘管開口！」

什麼都可以，是嗎？
那終究只是用來表示她心意多麼強烈的一句話。實際上，她不可能真的照實執行。不⋯⋯換個方式想，可以說正是因為信任對方，所以才能說出這樣的話吧？

「你不是在趕路嗎？不過⋯⋯就算再趕，你也要記得適度地睡覺休息。你看來就是一副只要放鬆，就會馬上倒下的模樣。」

我轉過身去。

「再會。」
「請等一下！雖然你說沒有必要，但是白白拿你的東西，實在太⋯⋯」
「哈提・斯庫爾是個天真得令人作嘔的男人。而且內心善良又溫柔。尤其對美女更是如此。」
「你、你騙人。」
「你這句話真過分耶。」
「不好意思。但是⋯⋯」

啊啊，對了。這女人，可以看穿大部分的謊言。
我嘆了一口氣。

「告訴你好了，我的目的並不是龍眼聖杯。」
「不是龍眼聖杯？」

我指著地板。

「我的目的是這下面的魔物。」
「我記得新的階層，好像就到龍眼聖杯所在的這個地方而已⋯⋯」
「我在某個地方，發現了過去偉人遺留下來的文獻。根據文獻上的資料，這下面有一隻魔物，擁有著對現在的我而言不可或缺的東西。而我前來此處時，正好碰上侯爵招募傭兵要探索這座遺跡，所以才順便報了名。」

彌絲朵用手摀住嘴巴。

「如果是這樣的話，我多少能夠理解⋯⋯可是──」
「現在的我並不缺錢。所以，我並非無論如何都要拿到龍眼聖杯不可。」

除此之外，我還有其他放手的理由。
龍眼聖杯是侯爵多年來死命尋找的東西。萬一我立了功，獲得款待，可就麻煩了。這女人好像很需要錢。把聖杯推給她正好。

我從記憶中喚起《禁術大全》裡記述的內容。
大賢者的筆記。祭壇和龍人的石像──有這兩種物品的房間。
如果筆記寫得沒錯，這房間裡應該有隱藏的階梯才對。我蹲下調查祭壇後方。
這一帶照理說有突起的地方⋯⋯喔，有了。
我按下突起的地方，同時向左轉動──

「請問？」

嗯？她怎麼還在？
我再次站起身來。

「怎麼了？」
「你要從這裡繼續前往下面的樓層，對吧？」
「啊⋯⋯你可以為我保密，不要告訴侯爵嗎？萬一他問東問西，解釋起來很麻煩。我已經把龍眼聖杯給你了，這麼一點小事，你應該可以為我做到吧？」
「我懂了。」
「對了，你沒有聽說發生了變異的事嗎？」
「我聽到了。但是我覺得這反倒是個好機會。」

原來是想趁機行事啊。

「鼎鼎大名的劍虎團沒有進入新階層就直接折返的事，在遺跡裡擴散開來，好像有不少傭兵因此決定暫時回到地面上。」

看來劍虎團的影響力還真是驚人。彌絲朵說完之後，將手按在胸上。

「哈提大人⋯⋯能不能至少讓我當你的護衛呢？」
「⋯⋯什麼？」
「就算幫你提行李也沒關係。再說，進入未知的領域，應該也有危險才對。身為一名劍士，我對武藝還是有自信的。就我看來，哈提大人你擅長的應該是使用術式。這麼一來，就免不了精神疲勞的問題。但是，如果有劍士在，使用術式的次數減少，也就能減少精神疲勞的問題。請你放心，我發誓，絶對不會成為絆腳石。」

彌絲朵向我靠近，仰頭望著我。

「你意下如何？」

精神疲勞度。MP在實際生活中，應該就是用這個詞來形容吧？
只不過──我現在不需要擔心MP多寡就是了。

「⋯⋯⋯⋯」
「啊──非常抱歉。」

可能是一時情緒激動，不小心靠得太近了。彌絲朵尷尬地往後退。

「但是，我還是不能白白收下你的龍眼聖杯。」

彌絲朵將視線轉向地板上的龍頭。

「照現場的情況來看，守護在這裡的魔物，好像也是哈提大人你打倒的⋯⋯」
「如果有你想要的材料，那顆頭隨便你拿走。」
「就──就說了，我不是想從你那裡得到好處嘛！」
「如果我告訴你，我想要自己一個人走呢？」
「這樣，我會一直過意不去的。至少讓我做些什麼吧。」

或許是因為睡眠不足，導致判斷力也變得遲鈍了吧？

「你不是在趕路嗎？」

彌絲朵稍微思索了一下。

「只要得到龍眼聖杯的報酬，旅費的問題就能一口氣得到解決了⋯⋯也能走完今後的旅程吧。這麼一想，稍微延遲幾天，也不是什麼問題。」

她並沒有想要讓步的意思。態度非常頑強。看來她原本就是重情重義的個性吧？
接下來⋯⋯該怎麼辦呢？
我想她應該是個──值得信賴的對象。我們這幾天下來，多少也累積了一些信賴關係。

⋯⋯劍士，是嗎？

有一個能夠近身搏鬥的成員，在這樣的狀態下戰鬥，會是什麼情況？我也很想試看看。
正好也能趁這個機會，向她詢問有關這個世界的各種情報。
或許是察覺氣氛有異，嗶嘰丸屏息靜待，不發出任何聲響。

「我可以開條件嗎？」
「可以。」
「基本上，請避免詢問任何有關我個人隱私的問題。請記得我們只是護衛和雇主的關係。」
「我明白了。」
「還有，我未必會在一天之內回到地面上。所以就算你到時候得一個人回到地面上，我也不負責。如果這樣也可以──那就拜託你擔任護衛了。」

彌絲朵露出安心的表情。

「謝謝。」

她立刻又恢復緊繃的表情。聲音也變得冷靜多了。

「哈提大人，我一定會好好保護你的，不計代價。」

她眼睛底下的黑眼圈，還是令人非常在意。而且仔細觀察，她的臉色也不太好看。
她還是一樣睡眠不足嗎？

「⋯⋯⋯⋯」

說不定用那個技能，讓她在某個地方稍微睡上一覺會比較好。
我依照筆記上寫的步驟，操作祭壇後方突起的部分。
結果，房間門關上了。這也如筆記上所寫的一樣。
接著經過幾秒，祭壇左右分離敞開，祭壇原本的位置出現一道開口，裡頭有通往下方的階梯。彌絲朵驚訝地杏眼圓睜。

「這種地方居然有隱藏的階梯⋯⋯」
「正如我剛才所說的，關於下一層的事，請不要報告侯爵。討厭麻煩的你，應該明白我的意思吧？」
「是的，我發誓絶對不會泄漏出去。」

我們開始走下樓梯。寬度正好供兩人並肩行走。我高舉發光的皮囊照路。

「好奇怪的燈啊。」

傳來一道戰戰兢兢的詢問。
大概是在意著我剛剛提及「盡量不要涉及個人隱私」的條件吧？

「這是一個很貴重的珍品。」

我並沒有說謊。
彌絲朵輕輕觸摸她額頭上的護帶。護額開始發光，照亮前方。

「那是這一帶常見的照明道具嗎？」
「不是，我想應該沒那麼常見。」

也就是所謂的魔法裝備嗎？
過了一會兒，我們抵達階梯底部。前面接著一條有遺跡風格的通道。
不同於廢棄遺跡下層廣闊的洞窟風格。而是文明尚存的領域。

「就現況看來，好像沒有魔物呢。」
「哈提大人，老實說──」
「什麼？」
「啊，不⋯⋯抱歉。沒什麼。請你別放在心上。」

這種時候的「沒什麼」，絶對有什麼才對。
不過，算了。我想，她原本一定是想問些有關我個人隱私的問題吧？
我們繼續往前進。

「有岔路耶。」

這一層看起來應該不是迷宮，但是⋯⋯還是先把路記住吧。
我們繼續往前進，這時長袍下的嗶嘰丸輕輕戳了我的側腹部。嗶嘰丸在不發出聲音的情況下，告訴我「那個」出現了──魔物的氣息。
魔物發出巨大吼叫聲，從轉角另一側現身了。
我第一次看見這種魔物。它的頭部令人聯想到花蕾。花蕾外側有三顆眼球。三顆眼球都是金眼。只有身體呈現人型，看起來非常怪異。有種不同於上面幾層魔物的詭異陰森。
彌絲朵拔劍往前走，用身體護著我。

「這裡就交給我吧。」

魔物頭頂打開。宛如綻放的花朵。
裡頭猛然飛出好幾條觸手。觸手對彌絲朵發出攻擊。
唰！唰！唰咻──！

彌絲朵手中的劍刃，準確地一一砍落觸手。
她邊砍下觸手邊前進。步法相當華麗。
轉眼之間，彌絲朵已經逼近到魔物眼前。接著──

唰！
一刀將魔物砍成兩半。

「咿、嘎啊啊啊啊啊啊────！」

魔物發出尖銳的慘叫，接著沉靜下來。
彌絲朵甩掉劍身上的血，將劍收回劍鞘中。我慢慢拍著手。

「真是精彩啊。」

彌絲朵對我輕輕一鞠躬。

「不敢當。」

嗯，有可以近身搏鬥的伙伴參與戰鬥──原來是這種感覺啊。
可以將自己顧慮不到的事交給她，或許不錯。
嗶嘰丸負責把風，已經是極限了。況且它的戰鬥能力還很低。

──護衛，是嗎？

「那麼我們繼續往前進吧。哈提大人。」
「好。」

仔細一看，這裡的空間比上面樓層寬闊。另一方面，構造看起來則非常簡單。
也有很多可以當成標示、辨別方位的牆面裝飾或柱子。這樣應該就不用擔心迷路了。
我邊走邊對彌絲朵說「對了」

「什麼？」
「我想請教你一些有關魔物的事。」

我想知道金眼魔物和其他魔物的差異。
彌絲朵並未懷疑我為什麼要問這種問題，她回答我：

「一般認為，金眼魔物擁有較多『經驗值』。這座大陸的人，有時候也會將經驗值稱為『魂力』。」

彌絲朵也知道經驗值的概念。
異界勇者可以吸收自己殺害的魔物魂力。當地人似乎是如此認知的。

「對異界的勇者們而言，金眼怪物等同於『提升等級』──也就是最適合提高自身防護力的獵物。你聽說過異界勇者的事嗎？」
「知道一點。」

經驗值，還有提升等級。這世界的人民，似乎多少都知道一些有關異世界勇者的事跡。

「還有，相傳就算殺了人類也無法獲得經驗值。據說所謂的經驗值，是魔物才有的特性。」

之前的四人組，武藝看起來非常高強，但是殺了他們之後，我的等級並未提升。
這下我懂了，因為他們身上一開始就沒有經驗值。現在想想，似乎也很合理。萬一殺人可以獲得經驗值的話，說不定會頻繁發生以獲取經驗值為目的而殺人的事件。
舉例來說，以生人獻祭的名目，讓勇者殺了知名的戰士。這麼一來，勇者就能一口氣提升許多等級。如果這種事可以成真，混帳女神搞不好就會準備許多活祭品，讓勇者這麼做。

「金眼魔物也是寶貴的經驗值來源，是嗎？」
「據說是因為過去某個時代，勇者們四處橫行、過度狩獵捕殺，才導致金眼魔物逐漸潛入地下。」

站在魔物的立場來說，勇者稱得上是虐殺者。

「所以地下遺跡群裡，才會棲息著大量的金眼魔物，對吧？」
「沒錯，人們是這麼分析的。另外，相傳金眼魔物群有三分之一聚集在大遺跡帶，所以才會形成金栖魔群帶。」

魔物是為了逃離過去的勇者，才潛入了地底下。導致後來形成遺跡地下迷宮。若站在不同角度來看，勇者陣營也可以說是殘酷無比的殺戮者。

「魔物偶爾會離開遺跡，出現在地面上。因此，各國都在盡可能地管理好地下遺跡。」
「不能直接堵住出口嗎？」
「就算堵住了，它們也會在其他地方製造出口。只不過，它們有一種習性，就是只要有了一個出口，就不會再從其他地方跑出來。」

那麼，廢棄遺跡為什麼會堵住呢？我記得那裡的出入口全部都堵塞住了。

「但是──我聽說有少數封印力量非常強大的遺跡存在。由神祇親自封印，讓魔物上不了地面。由於那些遺跡不需要管理，因此平常都置之不理。」

來自神祇的封印之力。女神。混帳女神──原來如此，就是廢棄遺跡嗎？

「沒有金眼的魔物，大家是怎麼看待它們的？」
「就是普通的魔物啊？」

就算你說普通也⋯⋯

「有些魔物跟人類建立友好關係。金眼魔物的特徵是極其凶暴，除了它們之外，其實有不少溫和沉穩的魔物。」
「簡單來說，金眼魔物究竟是什麼樣的存在？」
「你知道邪王素這個東西嗎？」
「我只聽過名字。」

混帳女神召喚我們至此時曾經解說過。好像是隱藏在大魔帝體內的特殊魔素。

「相傳只要邪惡的根源出現，釋放邪惡的邪王素將會對整個世界造成影響。根據傳說，魔物受到邪王素強烈影響，就會變成金眼。不過，只是生性凶暴的魔物本能獲得了解放而已，原本個性溫和的魔物並未受到影響。這是從前大賢者安格林提出的說法，現在則廣為世人所知。」

⋯⋯大賢者。我原本就預想到他很不簡單，但沒想到他似乎是個相當了不起的大人物。

「相傳大賢者本身也跟魔物締結了友好關係。聽說在魔物之中，他最喜歡的是史萊姆。」
「⋯⋯⋯⋯」
「怎麼了嗎？」
「真是有趣的故事呢。」
「？」

原來如此⋯⋯跟魔物締結友好關係的情況很普通，是嗎？
而金眼的經驗值較高，相對之下也很凶暴。
我將視線垂向肩膀處。
照這麼說來，將嗶嘰丸展示給彌絲朵看或許沒問題吧？
但是，話說回來──

「⋯⋯⋯⋯」

金眼魔物。
金。
金色勇者──桐原拓斗。
或許兩者之間其實沒有任何關聯性。
只不過，兩者一致擁有「金色」的標誌──對我而言，那是有些不吉的標誌。


◇【十河綾香】◇

十河綾香用力揮動連伽。
沉重的敲擊聲。緊接著，手腕傳來一股沉重的反作用力。
魔物骨頭上出現一道裂痕。綾香舉起剛才打在魔物身上的連枷，重新擺好架勢。
魔物的名字是骸骨。
骸骨動作停止，變成碎片。綾香擦拭汗水。

「呼⋯⋯」

這裡是位於亞萊昂王都附近的古代遺跡地帶。
據說被稱為魔骨遺跡。

2C的勇者們奉女神之命造訪這座遺跡。
為了習慣實戰，以及賺取經驗值。
主要目的是這兩個。

事前已被告知這裡是個魔物很多的地方。只不過，他們只獲准踏入遺跡的地下一層。但是這一帶有許多魔物跑出遺跡之外。因此，數量足夠。
然而，上頭也指示他們，通稱「角怪」的魔物出現時必須趕快逃命。
角怪的正式名稱是骸骨騎士。

這一帶超過９０％遭到死靈系的魔物佔據。其中多半是骨頭類的──也就是骸骨類的。一開始看見骨頭移動的光景，令她覺得詭異又陰森，但是已經習慣了。
綾香拿起事前插在瓦礫上的長槍，裝在皮製背帶上。
她現在手上拿的並非長槍，而是名叫連枷的武器。以鎖鏈連接著鐵塊和握柄部分。是一種利用離心力旋轉鐵塊，敲擊對方的武器。

面對骨頭類的對手，敲擊式的武器比刀劍更有效。
鬼槍流原本就是以精通百般武藝為目標的武術。
主力雖是長槍，但是也能應付無法使用長槍的局面。
鬼槍流武術設想的目標是戰場──簡單來說就是實戰型武術。哪怕是從敵人手上搶奪過來的武器，或者死者的武器。
只要能夠多用一種武器，生存機率就會跟著提高。能應付各式各樣的局面。

（話是這麼說，但這可是我第一次使用連伽⋯⋯）

在過去使用過的武器之中，投擲流星錘的攻擊方式，大概是最接近連枷的吧。
雖說如此，但是重心和撞擊的感覺卻截然不同。

『綾香你是天生的習武之才。但是⋯⋯出生在等同於不需要武藝的國家和時代裡，不知道──該說是幸運還是不幸啊。』

祖母曾說過這樣的話。

（奶奶，沒想到我現在來到了一個需要武藝的地方呢。）

「嘰噓咿咿啊啊啊啊啊啊──！」

手中握著短刀的骸骨從暗處跳出來。
可以自由操控人類武器的魔物。是靠著生前記憶在使用刀的嗎？
綾香看準時機，用力踏穩腳步。
碰！霹哩啪啦！
鐵塊粉碎了骸骨的胸部。
這種魔物身上有一塊顏色不同的骨頭，那正是它的弱點。只要擊碎弱點，就能輕易打倒它


～～～～～～～～～～～～～～～～～～～～

【技能等級已提升】
【LV4→LV5】


～～～～～～～～～～～～～～～～～～～～

確認活動狀態。

（還沒學會固有技能，是嗎？）

午後的陽光灑落在她身上。綾香獨自一人站在森林環繞的遺跡群中。

「⋯⋯⋯⋯」

2C學生現在分為好幾個團體。
首先是桐原拓斗的團體。Ｓ級的桐原擔任隊長。其他還有Ａ級的小山田翔吾，以及Ｂ級以上的勇者構成。感覺上是個人才濟濟、占盡優勢的團體。
接著是戰場淺蔥的團體。由Ｂ級的淺蔥負責整合隊員。最高等級是Ｂ級。只不過這個團體的人數很多。因為班上大部分的女生都加入了淺蔥組。成員也只有女生。
對綾香而言，這個團體裡面最令人放不下心的是鹿島小鳩。

（希望你平安無事，鹿島同學⋯⋯）

第三個則是安智弘的團體。這一組似乎是由無處可去的同學所組成的。就是那些遭到桐原組及淺蔥組拒於門外的學生。
成員中有兩名是Ｃ級。其他都是Ｄ級。Ｃ級以下的他們，似乎很想求助於Ａ級的存在。
但是，Ｃ級以下的女生全奔向了淺蔥的團體。
因此，組成這支隊伍的隊員全都是男生。這個團體的人數也很多。

第四組是高雄姊妹。Ｓ級和Ａ級。只有兩個人，或許很難稱之為團體吧。她們一直都以兩個人行動。雖說如此，但跟以前在日本時沒有什麼兩樣。
其他學生也都下意識地避開她們。不，應該說是盡可能地把她們當作不存在。姊妹兩人絲毫不在意其他人的態度。即使在異世界，那對姊妹依舊還是老樣子。她們不動如山的堅定意志，真值得我好好效法。綾香心想。

導師柘榴木和無法通過女神「試練儀式」的幾名男女，現在則留在城裡。
剩下的人，只有十河綾香。所有人都認定綾香是違抗女神的人。
跟她一起行動，會給女神留下不好的印象。大家似乎都是這麼想的。
那也無可奈何。大家內心會有這樣的想法，再自然不過了。

（別說他們，我也是遵從自己內心的想法採取行動的⋯⋯）

現在遭到所有人孤立的狀態，就是順從內心想法行動的結果。也只能乖乖接受。

「就叫你等一下嘛！明明是骨頭，行動卻這麼快！」「開什麼玩笑！你別逃跑啊！」「喂，死人骨頭！反正你已經死過一次了啊？那就乖乖讓我殺了你吧！」

熟悉的聲音逐漸靠近。看見他們的成員，便立刻明白是哪個團體。
桐原拓斗的團體。幾名穿著異世界服裝的男女停下腳步。

「搞什麼啊！這些死人骨頭，全部變成碎片了嘛！」
「啊！綾香？你該不會搶走了我們的骨頭吧？」
「咦？」
「好過分！綾香，你太過分了！那只骨頭是我們的耶！」
「你想打倒它，就要先取得我們的許可啦！不然我們不是白跑一趟了嗎！」
「就只因為你以前當過班長，就隨便搶走別人的東西，實在太蠻橫了！」

桐原組的成員紛紛訴說著心裡的不平。過了一會兒，小山田也出現了。

「這不是墜入邊緣人陣營的綾香大師嗎？唔哇～太過分了～竟然非法入侵我們的狩獵場～」

小山田用他肩上扛著的巨劍劍腹，輕敲自己的肩膀。簡直就像在挑釁一樣。

（狩獵場？）

綾香垂下視線。地板上用石頭畫了一條線。不怎麼清楚，只是隱約可見。

「這條線的這一側，就是我們的狩獵場喔～？綾香大師，你不是我們的成員吧？又不是我們這組的人！這很明顯是違法行為喔？糟糕了，綾香，你一下子就從班長淪落成犯罪者了嗎？好令人心寒喔～」
「夠了，翔吾。」
「拓斗，你是怎樣啦？你該不會是想幫綾香說話吧？」

最後，桐原登場。他身上穿著類似皮草大衣的外套。
金獅子。他的打扮給人這樣的印象。
桐原搖頭，說著：傷腦筋啊。

「十河再怎麼樣，好歹也是Ｓ級的。你們Ａ級以下的人再怎麼吠叫，她也不會輕易讓步。她根本就沒把你們放在眼裏。」

綾香立刻反駁。

「我沒有那個意思──」
「沒關係啦。」

桐原伸出手制止她。

「我已經聽薇希斯說過了。」
「你聽她說了什麼？」
「十河綾香的精神很脆弱。當初廢棄Ｅ級（垃圾）時，你採取的暴行，也是因為極度緊張導致精神錯亂的緣故吧？沒關係啦。我明白。」

桐原走向小山田等人前面。慢慢停下腳步之後，他繼續說：

「你現在還陷於錯亂當中，其實你並不知道自己在這裡幹什麼，對吧？」
「在你看來⋯⋯我是那個模樣嗎？」
「十河，其實我怕你怕得要命。」
「咦？」

（怕我？）

「我原以為你是我們2C最可靠穩重的人，沒想到卻變成這副模樣，帶給我很大的衝擊。只不過是被丟入異世界而已，那個可靠的十河綾香竟然就變得這麼奇怪⋯⋯」
「桐原同學，我告訴你，我到現在還無法相信那個女神──」
「十河，你聽好喔？你啊⋯⋯」

桐原站在十河旁邊，輕輕拍了拍她的肩膀。

「你在拒絶加入我們這支團體的時候，就已經不正常了。」

他看著綾香的視線中，隱含著憐憫之情。

「桐原同學⋯⋯」
「不過，你具備著Ｓ級的價值。只可惜，因為你腦袋不正常的關係，現在已經喪失了正常的判斷能力⋯⋯我真的覺得你很可怜。」

桐原轉過身去，他突然抬起下巴，似乎想到了什麼東西。

「如果我是國王──十河綾香，你配得上的身分就是守護國王的騎士吧？可惜⋯⋯你沒那個資格當上女王。」

桐原轉頭，臉對著綾香。表情上充滿絶對的自信。

「我會等你某一天清醒，乖乖加入我麾下，成為我的家臣的，十河綾香。」

小山田發出嗤嗤的笑聲。

「這場墜落谷底的好戲，真是傑作啊。你在2C的排名，也未免掉得太快了吧？班長前輩。」

綾香看了看其他桐原組成員的表情，他們臉上都露出奇妙的優越感。

「我是不太清楚啦，但是綾香搞不好靠古武術就OK了喔？」「說不定十河自己就能打倒大魔帝？」「可以、她可以！之前被女神一拳打中肚子就昏倒，一定是哪裡搞錯了！」「十河同學真的是最強！不愧是敢反抗女神的人！我們絶對模仿不來！」

綾香不發一語，轉身離開。

「喂，綾香！」

小山田叫住她。

「幹嘛？」
「只要你肯付錢，我們可以讓你在規定時間內進入前面那座狩獵場喔？其實，淺蔥她們付了錢，正在前面那片會不斷出現死人骨頭的狩獵場累積經驗值。」

所以桐原組才會暫時折返啊。

「我手頭上的錢並不多。」
「啥？之前女神不是才給了『零用錢』嗎？」
「⋯⋯我並沒有拿到什麼零用錢。」
「不會吧？哈哈，女神會不會太討厭你了啊！真可怜！好慘喔～」

綾香握緊拳頭。她對反抗女神一事，從未感到後悔。

「呀啊啊啊啊啊啊！」

突然傳來一陣慘叫，綾香回過頭去。
森林的另一邊傳來好幾聲哀號，同時有某個東西朝這裡接近。
人影。女生們。如鳥獸散般朝這裡四處竄逃。

「出、出現了！出現了出現了啦！不要啊啊啊啊啊啊！」

戰場淺蔥組。她們原本正在前面累積經驗值。

「怎、怎麼了？」

桐原組紛紛擺出對戰架式。淺蔥跑了過來。

「笨蛋！一般看這狀況就知道發生事情了吧！出現了！之前說的那只『角怪』出來了！」

（骸骨騎士？）

綾香舉起武器，突然睜大了眼睛。

「啊！」

因為鹿島小鳩攙扶著一個腳步踉蹌的女生，朝這裡跑來。
好像有人腳受傷了。

「呼啊⋯⋯呼啊⋯⋯我竟然淪落到讓小鳩來救我⋯⋯笑死人了⋯⋯」
「不用擔心！我們已經跟桐原他們會合了！你看，十河同學也在那裡！」

小鳩認出了綾香。看見自己的小鳩，露出安心的表情。
綾香看見小鳩的模樣，意識完全切換成戰鬥模式。
就在剛切換完意識之後。

「嗚啊啊啊啊啊啊⋯⋯啊啊～⋯⋯」

一個女生從森林裡跑了出來。背後跟著一個看似人類骸骨的巨大身影。
目測約有三公尺高。頭上長著一根令人害怕的角。
小山田後退。

「呃！不會吧？」

桐原組其中一個人伸手摀住嘴巴。

「嗚！」

晚一步才從森林裡跑出來的女生。

「啊啊啊⋯⋯沒了⋯⋯不見了⋯⋯不見了啦啊啊～⋯⋯」

左手手腕前方，消失無踪。
晚一步才出現的女生──佐倉麻美。她用右手抓著被砍斷的左手手掌。接連不斷的衝擊，襲向2C的學生。

「嘰噓咿咿啊啊啊啊啊啊──！」「嘰噓咿咿啊啊啊啊啊啊──！」

手上拿著寬劍和巨大盾牌的人形巨骨──兩只骸骨騎士。
綾香冒出雞皮疙瘩。它們呈現出來的「氣息」，跟先前的魔物截然不同。
它們比之前遭遇的任何一隻魔物都更強大。用肌膚就能感受到它們帶來的威脅。
綾香看向自己手中的連枷。

（用這個武器行嗎？不⋯⋯怎麼樣都得試看看！一定要爭取時間，讓大家逃離現場──）

「綾香你可不准出手喔啊啊啊啊啊啊！」

小山田大吼。

「這兩個死人骨頭，還在我們的狩獵場裡，是我們的獵物！你可別進來妨礙我們，Ｓ級窮鬼！」

桐原組其中一個成員驚訝地說：

「等等！小山田？上面不是交代我們，這傢伙出現的話就快逃命嗎！」
「沒關係啦。」
「桐原⋯⋯？」
「沒用的小卒才需要逃命吧？但是我可不同。我跟你們這些沒用的小卒，等級不一樣。」

（──不管他們怎麼說，照這情況看來，還是得幫他們一把才行！）

綾香無視小山田的話，準備往前衝。就在此時──

「不、不好意思，十河同學！」

小鳩將腳受傷的女生交付給綾香。

「你可不可以暫時幫我保護一下間宮同學？」
「咦？好、好的⋯⋯」

小鳩衝向失去手掌的麻美。
大家都逃跑了，她卻義無反顧地往前衝。不對──她也一樣一臉蒼白。肩膀和聲音都在顫抖。
但是，她卻無法棄之不顧。

「佐倉同學，我幫你把手的這裡綁起來喔？這種傷如果不止血的話⋯⋯！」
「嗚呃呃呃呃⋯⋯我受夠了啦啊啊啊⋯⋯」
「女、女神說不定可以幫你治好的！」
「討厭啦⋯⋯我想回去原本的世界啦啊啊⋯⋯」

麻美的嘴唇變得蒼白。小鳩俐落地解開腰帶。
她緊緊綁住麻美的手臂。麻美大聲哭叫。

「好痛喔喔喔！」

先前小鳩交給自己的女生──間宮誠子，臉上的表情扭曲。

「鹿島什麼時候變得那麼有膽識了？她那麼努力，反而讓人覺得噁心⋯⋯」

綾香不禁狠狠地瞪著誠子。

「怎、怎樣啦？」
「鹿島同學很了不起喔。」

桐原走向前方，跟小山田並肩站在一起。

「翔吾，我們一人一隻。」
「不用你說，我也知道！角怪，老子現在就過去殺你！」

小山田揮起大劍，拋了出去。
右邊的骸骨以盾牌彈開劍。
此時，小山田已經鑽入骸骨騎士的盾牌內側──也就是它的懷裡。

「【赤拳彈】！呃喔啦啊啊啊！」

小山田的固有技能。
他的能力，是從拳頭射出類似紅色子彈的巨大能量。
骸骨騎士高高舉起大劍。比它揮劍的速度更快──

轟隆！
紅色子彈狠狠撞擊骸骨騎士。

「嘎嘰咿咿！」

骸骨騎士左右晃動。

「看我的！【赤拳彈】、【赤拳彈】！追加追加追加！【赤拳彈】、【赤拳彈】！看我的看我的看我的啊啊啊啊啊啊！【赤拳彈】啊啊啊啊啊！」

快速發射固有技能。

「嘰、嘶！嘰、咿咿咿！咿、呃？嘶、嗚、咿呃！嘰嘶、呃呃！」

骸骨騎士承受不了衝擊，終於雙膝跪地。
幾秒後──骨頭魔物變成了碎片。粉碎的骨頭散落一地。
小山田握緊左右雙拳，仰天長嘯。

「太好了！等級提升吧啊啊啊啊啊！」

他旁邊的桐原早就用【金色龍鳴波】消滅另外一隻骸骨騎士了。

「骸骨騎士絶對不是什麼脆弱的魔物。這傢伙無庸置疑是個強敵⋯⋯但是，我比它更厲害。我總是對名叫桐原拓斗的這個人──對我自己感到驚訝⋯⋯」

桐原組振奮地歡呼。

「不愧是桐原！」「桐原果然帥呆了～♪」「Ｓ級勇者不是虛有其表！」

桐原哼了一聲。

「這麼一來⋯⋯就到達等級２４了。」
「不好意思～讓你久等了～」

女神薇希斯從房間深處走了出來。房間裡的地板較室內矮了一階。十河綾香就坐在房間裡的椅子上。
據說這裡是女神的第二個房間。房間深處擺滿了書桌和高高的層架。
書桌上堆疊著成束的紙張和信函。女神坐在正對面的椅子上。

「抱歉把你叫來這裡，十河殿下。」
「你找我有什麼事？」

從魔骨遺跡回來之後，綾香被女神叫了出來。

「呵呵呵。」

現在看見女神的笑容，綾香也忍不住擺出防衛的架勢。女神拿出一個小袋子放在桌上。

「我一時疏忽，忘記把這個交給綾香殿下你了。都怪我太忙碌了。真是抱歉。你貴為Ｓ級勇者，一定很需要錢吧？」

這好像就是之前小山田提到的「零用錢」

她是真的忘記了嗎？綾香忍不住以懷疑的眼神看著她。

「還有，你今天前往魔骨遺跡，我沒有陪著你一起去，真是抱歉。」

女神看向背後的紙束。

「因為大魔帝軍隊突然往南前進，還有大誓壁陷落的關係，害我工作增加不少～各地傳來的報告，我怎麼看都看不完。所以，拖這麼久才能確認重要度較低的地區情報～」
「不能拜託其他人篩選情報嗎？」
「呵呵，拜託別人篩選之後，還有那麼多的量呢。換個話題吧。」

女神切換話題。

「骸骨騎士出現，好像引起了一陣混亂，對吧？」
「請問，佐倉同學呢？」
「啊啊，她治好了喔？我先用【女神氣息（HEAL）】將她被砍斷的手掌接回去了。」
「這樣啊。謝謝你，女神⋯⋯」

（太好了⋯⋯）

「不過呢，神力不是誰都會用的喔？尤其是【女神氣息（HEAL）】用起來非常累人。但是沒辦法，因為佐倉殿下是Ｂ級勇者呀～」

這句話言下之意，是在表示「Ｃ級以下的，我說不定就不會治療了」的意思嗎？女神露出微笑。

「對了，十河殿下⋯⋯你好像遭到大家孤立了，不要緊嗎？我很擔心這點⋯⋯」
「現在我一個人還可以。」
「呃，那個⋯⋯冒昧請問一下，十河殿下你有自覺嗎？」

綾香輕嘆了一口氣。

「你是在問，我腦袋是否還在錯亂嗎？」
「啊，不是不是！我看你現在比之前穩定多了，而且我們也能溝通。啊，桐原殿下他們對你說了什麼嗎？嗯～舊情報還在2C的各位之間流傳，這狀態可真令人傷腦筋呢～」

女神露出煩惱思索的模樣。

「看來需要某個具有影響力的人，來訂正舊情報才行呢～」
「請問，關於我有沒有自覺的事⋯⋯」
「啊，要聊那件事嗎？嗯～那麼我首先問你一個問題。十河殿下，你不打算加入桐原殿下他們的團體嗎？就我而言，可以的話，很希望Ｓ級勇者盡量一起行動。」

綾香移開目光。她總覺得現在的桐原好像哪裡怪怪的。該說是有點瘋狂嗎？

「我不認為可以跟現在的桐原處得來。就現狀來說，要攜手戰鬥，是有困難的。」

女神露出微笑。

「就是那個意思。」

（就是那個意思？）

綾香不懂她這句話想說什麼。

「簡單來說，在我看來，我認為十河殿下的任性妄為，很可能會打亂2C各位同學的腳步。」
「任、任性妄為？」
「啊，哎呀？我說錯了嗎？就我所聽到的，你採取行動時，受個人情感要素影響的成分很大⋯⋯哎呀呀？難道不是嗎？」
「這──」
「你可以有條有理地說明給我聽嗎？不是含糊籠統的預感或擔憂，而是有邏輯的說明。沒問題嗎？」
「那個──」
「嗯呵呵？解釋不出來的話，表示你的行動果然就是出於任性吧？嗯～但是這樣我會很傷腦筋呢。高雄姊妹任性妄為的行動，也很引人注目⋯⋯十河殿下你也是，我原以為只要誠心誠意待你，就能講得通，沒想到連你也變得跟任性的小孩一樣⋯⋯」
「才、才不是！我只是⋯⋯」
「不用解釋，沒關係。」

女神眼角泛淚。

「全怪我指導能力不足，才會引起這個情況⋯⋯嗚嗚⋯⋯我得負起全責⋯⋯」

綾香站起身來。

「十河殿下？」
「很抱歉。就算是我任性好了，我無法跟現在的桐原同學他們一組。」
「你不惜轉身背棄尋求救贖的這個世界，也拒絶跟他們合作嗎？」
「我打算用我的方式，完成Ｓ級勇者的使命。」
「無論如何都不行嗎？」
「對不起。」
「我懂了。」

女神露出笑容，雙手輕輕一拍。

「那麼，我就將無法通過試練的學生們托付給你囉！」
「咦？」

她突然說什麼？她說的話，沒有任何脈絡可循。

「大家都在避著你，處於現在這種孤立的狀態，一定很辛苦吧？不過，無法通過試練的人，都是Ｃ級以下的就是了⋯⋯但是，不要緊。只要身為Ｓ級的你好好訓練他們，他們一定可以帶來幫助的，你說對不對？只不過呢──」

女神露出陰郁的表情。

「我會為你們祈禱，希望不要有人喪命⋯⋯」
「你、你幹嘛突然提那種事？再說，我會代替他們上場戰鬥，這件事之前不是早就講好了嗎？」

女神看似遺憾地垂下視線。

「其實前幾天，國王下了一道命令。」
「國王下了命令？」

女神無法違抗國王的命令嗎？但是⋯⋯總覺得好像哪裡怪怪的。

「國王表示『理應捨棄無法戰鬥的勇者』⋯⋯啊，我可是好好努力說服過他了喔？但是，國王根本充耳不聞⋯⋯真抱歉，都怪我力有未逮⋯⋯」
「⋯⋯⋯⋯」
「關於今後該怎麼處置他們，我也稍微煩惱了一下。不過，如果十河殿下你願意幫我照顧，我就放心多了！」
「⋯⋯⋯⋯」
「十河殿下？」
「萬一我拒絶的話，你會丟棄他們嗎？」
「我不得不那麼做。對不起。」
「我明白了。」
「太好了！那就拜託你了！不過，既然你任性地拒絶跟桐原殿下他們合作，幫忙照顧那些不及格的人，也是理所當然的吧。」

女神補了一句「詳細情況，我之後再向你說明」。臨走前，綾香向女神鞠躬。

「打擾了⋯⋯」

就在即將離開房間之前，女神叫住綾香。

「啊，這只是萬一啦──若到時候他們絆手絆腳，十河殿下你或許得選擇直視殘酷的現實。不過，你放心。瞭解現實的人才會變得更強。我相信人的可能性。人是一種會自我成長、改頭換面的生物⋯⋯等十河殿下脫離任性的孩提時代，蛻變成大人之後，我會幫你好好跟桐原殿下他們美言幾句的。我向你保證。」
「⋯⋯感謝你這麼替我著想。」「好！我很期待你的轉變喔！」
「我告辭了。」

綾香反手關上了門。


◇【女神薇希斯】◇

「臭丫頭！」
「────找人麻煩的猴崽子！」


◇【三森燈河】◇

走向下層的途中，我將嗶嘰丸介紹給彌絲朵。

「難怪之前附近明明看不見魔物，卻有魔物的氣息，害我一直很介意。原來是這樣啊。」

這句話讓我想起之前通過隱藏階梯時的對話。

『就現況看來，好像沒有魔物呢。』
『哈提大人，老實說──』

那時候，彌絲朵好像想問我什麼。大概是想告訴我有魔物的氣息吧？
也不能怪她，誰叫我一直將「嗶嘰丸之外的才是魔物」視為前提⋯⋯

「不用怕，嗶嘰丸。」
「──嗶！」

嗶嘰丸戰戰兢兢地朝彌絲朵的方向伸出突起。
彌絲朵也伸出手。她朝我一瞥。

「沒有問題吧？」
「對。」
「嗶嗶嗶～⋯⋯嗶、嗶⋯⋯嗶⋯⋯？」

嗶嘰丸仍在警戒著她。突起處輕輕戳了戳彌絲朵的指尖。
畏畏縮縮、戰戰兢兢的感覺。彌絲朵嘴角泛起微笑。眼神也緩和下來。

「我叫彌絲朵・巴魯卡斯。以後請你多多指教囉，嗶嘰丸大人？」
「嗶？嗶⋯⋯嗶⋯⋯嗶嗚嗚嗚～⋯⋯♪」

嗶嘰丸開始用突起處磨蹭彌絲朵的手指。
顏色變成淡粉紅。看來它對她很有好感。

「呵呵，這只史萊姆好可愛喔。跟它在一起，心情很不可思議地就放鬆多了。」
「嗶～♪」
「嗶嘰丸好像很中意你呢⋯⋯讓它這麼有好感的人，除了我以外，你應該是第一個吧？」
「嗶？嗶嗚嗚嗚～！」

突起處突然伸向我的方向。臉頰感受到一股軟綿綿的觸感。
粉紅色的色澤變得更加鮮艷。彌絲朵伸手摀著嘴巴，噗哧一聲露出微笑。

「即使如此，它最喜歡的果然還是哈提大人呢。」
「嗶♪」

那之後，魔物依舊不時向我們發動攻擊，但全部都被彌絲朵制伏了。
絲毫沒有任何危險的戰鬥。看來她的武藝果然高強。
該怎麼說呢？給人一種身經百戰的感覺。
我們繼續往下方階層前進。又來到一個很像居住區的地方。

「嗯？」

一座眼熟的門，映入眼簾。
正確來說，令人覺得眼熟的不是門，而是寶石。是那扇只要注入魔素就會自動開啟的門。
我觸摸寶石注入魔素，門順利無礙地打開。
我邊提防四周，邊確認內部。裡頭空蕩蕩的。
我走進裡面調查房間。沒有什麼特別令人在意的東西。只擺了一些老舊的家具──

「看起來似乎沒有陷阱的痕跡⋯⋯」

我對在門外把風的彌絲朵大喊。

「你可以進來了。」
「我知道了。那就失禮了。」

彌絲朵走進房間。她關上門、放下行李後，取出懷錶確認時間。

「我們在這裡稍事休息吧。」
「我知道了。」
「你可以稍微睡一下喔？要出發我再叫你。」

彌絲朵停頓了一會兒才回答。

「不，沒關係。」
「在我看來，你很明顯已經睡眠不足了⋯⋯既然你發誓不會成為我的絆腳石，可以的話，我希望你稍微打個盹也好。」

彌絲朵垂下目光。

「我想我一定睡不著。」
「我聽說只要躺著休息，多少也能消除疲勞。至少在地板舖上睡袋，稍微躺一下，你覺得呢？萬一你在中途昏倒了，我也很困擾。」

彌絲朵稍微思索了一下，接著臉上帶著微笑，嘆了一口氣。

「我知道了⋯⋯那麼，我就先躺一下吧。」

彌絲朵摘下發出淡淡光芒的護額，卸下身上的裝備。
接著，她舖好睡袋，躺了下來。身體背對著我。

「嗶嘰丸。」
「嗶。」

我先前已經偷偷給了嗶嘰丸指示。
要它引開彌絲朵的注意力。
嗶嘰丸跳下地板，恢復成圓形。直接爬向彌絲朵的正面。然後，在她的臉前面停止。

「嗶！」
「嗶嘰丸大人⋯⋯？你怎麼了嗎？」
「嗶！」
「──【SLEEP】。」
「──⋯⋯呼。」

彌絲朵中了我的【SLEEP】。這麼一來，在血條消失之前，她就能好好睡上一覺了吧？
深層睡眠的話，即使是短時間也具有恢復之效。以前，我在網路上的某個地方看過這說法。

「要是持續時間再長一點的話，說不定能用在治療失眠上呢⋯⋯」

不過，中了【SLEEP】之後是否具有安眠效果，至今仍舊不明就是了。

「嗶、嗶、嗶、嗶、嗶。」

嗶嘰丸左右晃動。它的動作看起來就像節拍器。
那個行動⋯⋯是在保護彌絲朵吧？就在此時──

「嗶？嗶！」

彌絲朵的護額消失了。
放在睡袋旁邊的護具也一併消失無踪。
緊接著，彌絲朵發生了變化。

「────────」

她的耳朵開始變形。
尖尖的長耳朵。

「精靈⋯⋯？」

我繞向橫躺的彌絲朵正面。

「嗶？」

嗶嘰丸就像在詢問我「這是怎麼回事？」

「老實說，我之前就有預感了。但是⋯⋯這真是太令人吃驚了。」

我仔細觀察彌絲朵。除了耳朵之外，其他部位也發生了變化。臉也變了。
我想可能是因為剛才的【SLEEP】，而解除了「某種事物」

頭髮也有了些微的改變。
長相明顯變得截然不同。這情景⋯⋯令我忍不住倒抽了一口氣。
變化前的彌絲朵，可以說是個美人。
但是，現在──遠遠超過那之上。
我以為我已經看慣美人了。好比說，嬸嬸。
以2C來講，就是十河綾香。除此之外，高雄姊妹也是所謂的美人胚子吧。
所以，我也一直認為彌絲朵屬於相同範疇的「美人」

但是，現在的彌絲朵・巴魯卡斯可不同。該怎麼說呢⋯⋯就像騙人的一樣。
與其說是生物，更像追求美麗到極致而產生的雕塑品──

『我聽說精靈不管男女都美得讓人瞠目結舌喔？』

我突然想起酒館裡的對話。

「原來如此⋯⋯這就是所謂的高等精靈嗎？」

接著，我想起一名人物──孟克・多羅格提。
在廣場上找彌絲朵麻煩，結果發現認錯人，顏面盡失的男人。後來在這座遺跡裡丟了性命。
那傢伙在廣場上糾纏彌絲朵之後，我總覺得不對勁。
孟克的確信，實在過了頭。
他為什麼那麼信心滿滿呢？那點也令我有點在意。
那時候，彌絲朵頭上戴著帽子。一般在那種情況下，應該不可能如此堅信不疑。因為他甚至還沒確認過對方的長相。

首先，理所當然會考慮眼前的對象可能是別人。
但是，孟克在那時，卻懷著堅決確信的態度。
為什麼？
因為他有──讓他深信的根據。
比如說，當時聽到聲音的當下，我就發現了。彌絲朵就是「我在暗色森林遇見的那個女人」。孟克跟我一樣，聽見彌絲朵的聲音後，也發現了她的身分。

她恐怕是藉著什麼特別的力量，使長相和耳朵產生變化。
但是，我想她能改變的，只有外貌。

「聲音」就無法改變了。
孟克對「瑟拉絲・亞休連」異常地執著。他清楚記憶著她的聲音，不可能遺忘。而她們的聲音一致。在孟克心裡，完全一致。

還有一點。
我的視線移向彌絲朵胸口。我想──她的體型並未發生變化。
就我所見，發生變化的只有頭部。我再次回想孟克的發言。

『你那對豐滿淫蕩、引人產生邪念的酥胸！我全記得一清二楚！』

這對「酥胸」也深深烙印在那傢伙的腦海裏，足以令他擁有那樣的自信。將胸部烙印在記憶裡的行為實在不可取，但是⋯⋯況且上面還覆蓋著衣服，他竟然認得出來。

不過這也表示，對孟克而言，那都是相當重要的個人特色吧。
聲音和胸部。強烈烙印在記憶裡的個人特色。正因為已經烙印在腦海裏，所以他才如此堅信。
也因此，拿下帽子的時候，孟克才會陷入混亂。
因為她的長相和耳朵的形狀，和他的記憶並不一致。
孟克一定不知道為什麼會如此吧。
聲音和體型完全一致。可是長相和耳朵卻截然不同⋯⋯

『怎麼可能⋯⋯騙、人⋯⋯騙人的，騙人⋯⋯』

他會陷入嚴重錯亂，我也可以理解。
我將視線移向尖尖的耳朵。
那是孟克想伸手觸摸彌絲朵時發生的事。
彌絲朵就像在斥責他一樣，狠狠撥開他的手。

「也就是說⋯⋯只有外貌改變而已嗎？身體本身並沒有產生變化，只有映照在對方眼中的『訊息』發生了變化⋯⋯？」

要說的話，就像幻術嗎？
原來如此⋯⋯萬一被觸摸到，「視覺訊息」和「手感」就會產生出入。
因此，她才會那麼強烈抗拒被人觸摸啊。

『順便問一下，精靈很厲害嗎？』
『據說可以藉用原始精靈之力戰鬥，所以很厲害。』

旅社酒館裡那群男人說過這樣的話。

「幻術是精靈之力嗎⋯⋯」

能看穿別人的謊言，說不定也是精靈之力。
護額、護具、盔甲。
全都是藉助精靈之力製造出來的裝備吧。
現在，彌絲朵的精靈之力好像已經解除了。
她之前因為【PARALYZE】動彈不得時，精靈之力的效果還能持續，但是⋯⋯

「看來，一旦中了【SLEEP】，精靈之力也會暫時消失呢⋯⋯」

我竟然在料想不到的地方，發現了【SLEEP】的優勢。
我低頭看著一臉安穩熟睡的美麗護衛。藍色的血條正在逐漸減少。

「⋯⋯⋯⋯」

我還不確定。
只能說原本懷抱的預感，得到了補強而已。
但是，這麼一來幾乎可以定論了吧。

從涅亞聖國消失踪影的前聖騎士團長。
高等精靈的公主。

彌絲朵・巴魯卡斯的真面目就是──

「公主騎士，瑟拉絲・亞休連嗎？」