# HISTORY

## 2019-12-23

### Epub

- [翼の帰る処シリーズ](record_out/%E7%BF%BC%E3%81%AE%E5%B8%B0%E3%82%8B%E5%87%A6%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA) - record_out
  <br/>( v: 10 , c: 42, add: 42 )
- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )
- [相隔１０１米的愛戀](ts_out/%E7%9B%B8%E9%9A%94%EF%BC%91%EF%BC%90%EF%BC%91%E7%B1%B3%E7%9A%84%E6%84%9B%E6%88%80) - ts_out
  <br/>( v: 1 , c: 54, add: 54 )

## 2019-12-21

### Epub

- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 49 )

## 2019-12-19

### Epub

- [人喰い転移者の異世界復讐譚　～無能はスキル『捕食』で成り上がる～](ts_out/%E4%BA%BA%E5%96%B0%E3%81%84%E8%BB%A2%E7%A7%BB%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E5%BE%A9%E8%AE%90%E8%AD%9A%E3%80%80%EF%BD%9E%E7%84%A1%E8%83%BD%E3%81%AF%E3%82%B9%E3%82%AD%E3%83%AB%E3%80%8E%E6%8D%95%E9%A3%9F%E3%80%8F%E3%81%A7%E6%88%90%E3%82%8A%E4%B8%8A%E3%81%8C%E3%82%8B%EF%BD%9E) - ts_out
  <br/>( v: 7 , c: 100, add: 41 )

## 2019-12-17

### Epub

- [乙女ゲー世界はモブに厳しい世界です](syosetu_out/%E4%B9%99%E5%A5%B3%E3%82%B2%E3%83%BC%E4%B8%96%E7%95%8C%E3%81%AF%E3%83%A2%E3%83%96%E3%81%AB%E5%8E%B3%E3%81%97%E3%81%84%E4%B8%96%E7%95%8C%E3%81%A7%E3%81%99) - syosetu_out
  <br/>( v: 8 , c: 182, add: 32 )

## 2019-12-14

### Epub

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( v: 2 , c: 25, add: 12 )
- [處刑少女的生存之道](girl_out/%E8%99%95%E5%88%91%E5%B0%91%E5%A5%B3%E7%9A%84%E7%94%9F%E5%AD%98%E4%B9%8B%E9%81%93) - girl_out
  <br/>( v: 1 , c: 10, add: 10 )

### Segment

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( s: 1 )

## 2019-12-12

### Epub

- [漆黑使的最強勇者](syosetu_out/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85) - syosetu_out
  <br/>( v: 2 , c: 22, add: 0 )
- [異世界迷宮都市治癒魔法師](wenku8_out/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BF%B7%E5%AE%AE%E9%83%BD%E5%B8%82%E6%B2%BB%E7%99%92%E9%AD%94%E6%B3%95%E5%B8%AB) - wenku8_out
  <br/>( v: 4 , c: 57, add: 57 )

## 2019-12-10

### Epub

- [漆黑使的最強勇者](syosetu_out/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85) - syosetu_out
  <br/>( v: 2 , c: 22, add: 22 )

## 2019-12-04

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 96, add: 0 )



