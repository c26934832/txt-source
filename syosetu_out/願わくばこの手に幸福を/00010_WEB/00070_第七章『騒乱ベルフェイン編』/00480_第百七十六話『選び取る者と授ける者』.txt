啊，該怎麼稱呼這種感情？該給它起個什麼名字？

能用這雙眼睛看見她的身姿真的是萬幸。那聲音宛若天賜。所以，所以。正因為如此，現在心底萬分的憤慨。

「──喂喂，饒了我吧。我迷上的女人，可不會像你這樣說爛話啊。」

連骨髓都被熱量侵蝕了，呼出的氣已經達到了無法想像的熱度。明明剛才為止還一直在吐冷氣來著⋯但是。不知從何處滲入的熱氣，吞噬著冷氣。

原本朦朧的視野，現在清澈得驚人。憤怒，激憤。啊啊，不，那樣的言語，怎麼能表達出在心中焦躁不安的感情？

不知道理由。到底發生了什麼事，我怎麼可能知道。唯一能明白的是，現在在和我說話的，一定不是阿琉珥娜，而是全然不同的某人。即使那具身體，確實是阿琉珥娜本人的。
正因如此，胸口才會發燙。正因如此，才想要撕裂那喉嚨。

對。阿琉珥娜，我所思念的人，是絶不會說努力是徒勞這種話的。她看到人們奮勉拼搏的樣子，絶不會露出嘲笑的神情。無論是在孤兒院度過的時光，還是在過去的旅程中都不曾改變。

轉過身來，用眼睛捕捉她的身影。其身姿，實在不得不令人認為是阿琉珥娜本尊。但是，在有些方面存在著致命性的不同。該說是精神呢，還說是靈魂？雖然眼睛看不見，但是塑造出阿琉珥娜其人的部分根基，眼前的存在確實有所缺陷。

凝視著那不知名的女人，張開了嘴唇。

「怎麼把嘴閉上了？笑啊？接著笑啊？你剛才不還笑的很歡麼？就好像我只能那麼做一樣。」

啊，是啊。只有那些不知努力的人，才會嘲笑他人的努力。嘲笑他人流血的樣子的，只會是那些不敢流血的膽小鬼。

而阿琉珥娜，絶對不是那樣的人。是的，我相信。不管是現在，過去，甚至未來。

「虛張聲勢的膽小鬼。雖然不知道是誰，但直到生命逝去的那一刻為止我都會嘲笑你。畢竟你是個不借別人的身姿就無法把話說出口的卑鄙小人啊，嗯？」

就像濁流一樣，言語不斷從嘴唇中吐露出來。
這和剛才還在耳邊響起的動人言語完全不同。只是從嘴唇吐出感情，只是感情的羅列。
但是這樣正好。語言這種東西，如果總是被理性所束縛，就太沒意思了吧？

那樣灼熱的言語全部吐露出來之後。

唐突響起的，是和到剛才為止，阿琉珥娜式的遣詞用句、阿琉珥娜式的語調完全不同的某人的聲音。

──果然很像呢。在討厭的方面也是，可愛的方面也是。奧菲會握住你的手也可以理解了。

無法回應那句莫名其妙的話。雖然不明白意味，但卻奇怪地縈繞在耳邊。

所謂的奧菲，是什麼？我記得紋章教的神好像就是那個名字。但是為什麼，那個名字會出現在這種場合？我怎麼也不明白。不知名的某人的聲音，再次回響在耳邊。

「你現在為什麼會感到憤怒？啊啊，是這具身體吧。誒，聖女是和我相近的東西。那麼，就算借用下身體也沒有問題。」

這具身體，聖女，借用。那些單詞在腦海中浮現，然後消失。這傢伙在說什麼啊？我不明白，什麼都不明白。

發燙的頭腦似乎拒絶處理這些信息。腦海中雖然浮現出了一些可能性，但卻自己消除掉了那些想法。──不可能，不可能有這種事。

與完全不能使用的大腦相反，身體做出了行動。不由自主地張開半身，右手的小指掛在腰前的寶劍上。眼睛像脈動一樣地顫抖著。

「別這樣，別把手指掛在那把劍上，這不是很嚇人麼？我是個膽小鬼。請你不要再這樣了好嗎？」

眼前的這個人，以阿琉珥娜的身姿說出的話，相當輕薄。一點真實感都沒有。對話到底能否成立，我也不太清楚。
不管怎麼說，言語中應該存在的分量，在她嘴唇中溢出的聲音中完全感受不到。真的只是單純地發出聲音？輕描淡寫的話語，完全不會讓我感到威脅。
明明如此。眼睛卻不住地痙攣，心臟不停地跳動，腳也蜷縮著。身體怎麼也擺不好姿勢。

「啊啊，但是。反正你什麼也做不了──不管怎麼說，這具身體是屬於你思念之人的東西，對吧？」

那個傢伙在我的面前，就在自己的身邊，這樣說道。伸出那白皙的脖子，嘲笑著。啊啊，真是有趣，真是愉快。

大腦發暈。骨髓燃燒起來，覆蓋全身的疼痛消失了，指尖深深地挖著掌心的肉。

「對了，對了。你知道嗎？我的興趣是寫劇本。雖然自己說有點那個，但這次也會寫出好的劇本哦。沒錯，無論是誰，都會得到拯救，誰都會得到幸福。就是這樣一個既幸福又愉快的故事。有英雄，有勇者，有聖女。最後，大家都歡笑著。」

她一邊轉移著話題，一邊歡快地笑了起來。用白而細的手指劃過我的下巴，好像在玩弄著我似的。

咬緊的牙齒，發出宛如破碎般的聲音。下巴發生了前所未有的摩擦。如果不這樣做的話，我可不覺得能壓制住這扭曲身體的激情。

「在這個故事中，我決定了你的角色，非常好的角色。只有你能演好，你會起非常重要的作用。當每個人都沉浸在幸福中的時候──」

一邊嘟噥著，眼前的存在睜大了眼睛。黃金的光輝窺視著我的臉，在嘴唇快要重疊在一樣的距離，吐露出言語。

──你一個人，絶望地去死吧。

像祝福一樣。像詛咒一樣。那個女人的手推開睜大了眼睛的我的身體。我明白自己的身體失去了重量。最後的最後，女人看著向下墜落的我，喃喃自語般說道：

──所以至少，把眼前的問題輕鬆解決掉吧。