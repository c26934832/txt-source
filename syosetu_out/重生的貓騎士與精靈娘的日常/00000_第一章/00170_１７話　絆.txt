「呋呋〜、氣持ちいいです⋯⋯」

酒瓶片手に。
虛ろな目で艾麗婭が言う。

機の上には空いた酒瓶が３本ほど⋯⋯

飲み過ぎである。
よっぽど塔瑪がペットになったのが嬉しかったようだ。

當の塔瑪はというと。
艾麗婭の飲みっぷりに呆れ果て。
相手してられないとばかりに、ベッドでペロペロと毛づくろいをしている。

「もう、ご主人樣を放っておいて毛づくろいなんて⋯⋯塔瑪は冷たいのですね⋯⋯⋯そんな態度をとってると、一緒にお風呂に入ってあげませんよ？」

（お風呂⋯⋯だと⋯⋯ッッ！？）

聞き捨てならない言葉に。
塔瑪の動きがピタリと止まる。

そんな塔瑪をよそに。
艾麗婭は、機の上の空き袋を片付けると。
ベッドの橫においてあった籠の中に、著替えやタオルなど詰め込んでいく。

「一緒に行くなら今のうちですよ〜？」

ちょっとさみしそうな視線で、塔瑪に籠を向けてくる艾麗婭。
行くなら、この中へ入れ⋯⋯ということらしい。

（し、仕方あるまい。我が輩はご主人の騎士。いついかなる時であろうと、その身を守るために離れるわけにはいかぬ。そう、これは仕方のないことなのだ！）

誇りある騎士として、女風呂に入るなど言語道斷。
しかし、絶世の美少女である自分の主人に、いつ劣情を催した男が襲いかかるとも限らない。ならばここはプライドを捨て、ご主人の警護に努めるべき⋯⋯⋯

そんな言い訳で、無理やり自分を納得させると。
塔瑪はピョンっと籠の中に飛び降りた。

⋯⋯⋯⋯その飛びっぷりは、ずいぶんと輕やかだった。


◆

「あら、艾麗婭醬。お風呂かい？」
「はい。ちょっと遅くなってしまいましたが、使っても大丈夫ですか？」

２階の部屋から降りると。
料理の載ったトレーを運んでいた女性が、艾麗婭に話しかけてくる。

この宿屋は２階が宿泊施設。
１階が、受付兼酒場となっている。
艾麗婭に話しかけた女性は、この宿の女店主だ。

「ああ、使って大丈夫だよ、ちょうど艾麗婭醬で最後だ。ゆっくり使っていいよ。貓醬もしっかり洗ってやんな」
「ありがとうございます！」

女店主の氣遣いに。
艾麗婭は禮を言うと、宿の奧へと進んでいく。
ここには交代利用制だが、それなりの大きさの洗い場と湯船が設けてあるのだ。

艾麗婭が浴場へと向かっていくのを見て。
酒場で飲んでいた男の１人が、あとを追おうとするが、それに氣づいた女店主のゲンコツによって阻止される。


◆

（う⋯⋯浮いている⋯⋯）

脫衣所で、艾麗婭の魅惑の脫衣シーン。
そして、いちゃいちゃにゃ喵呀ん洗いっこを堪能した塔瑪。

艾麗婭の體は、その體型も完璧であった。
さらに肌にはシミひとつなく、まさに理想の肌だった。

だが、そんな光景さえも吹き飛ぶようなことが、塔瑪の目の前で起きていた。

浮いている。
艾麗婭のふたつのメロンが湯船にプカプカと浮いているのだ。

自分も湯船に浮かびながら。
塔瑪はその光景に目を奪われていた。

「塔瑪⋯⋯？」
「にゃっ！？」

酔いと湯船の溫かさで、天井をぼーっと見上げていた艾麗婭が、急に塔瑪に話しかける。
まさかメロンを見ていたのがバレたのかと、塔瑪は慌てた樣子で返事をする。

「塔瑪⋯⋯わたしは強くなれるでしょうか？」
「⋯⋯にゃあ？」

艾麗婭問いに。
塔瑪は心配そうな面持ちで鳴き聲を返す。
彼女の瞳が不安そうに揺れていたからだ。

すると、艾麗婭はポツポツと語り始めた。

「わたしの住んでいた国はね。昔、魔族の軍勢に襲われたことがあったんです。その時、わたしは小さくて戰うことができなくて、ずっと隠れていたの⋯⋯でもね、魔族は鼻がいいから、そんなわたしはすぐに見つかっちゃったの⋯⋯」

そこまで言うと。
艾麗婭はその時のことを思い出したのか、身震いしてしまう。

だが、それも束の間。
次の瞬間には彼女の顏に笑顏が生まれる。

「でもね、そんな時に現れたの、『剣聖樣』が⋯⋯！剣聖樣は、魔族に追い詰められたわたしの前に現れると、二本の刀で次々と魔族を斬り捨てて、わたしを救ってくれたんです」

（剣聖⋯⋯⋯もしや、あの剣聖か？）

艾麗婭の言う、剣聖という名に。
塔瑪は聞き覺えがあった。

剣聖とは⋯⋯數年前に、魔族の軍勢がとある国を襲った際に、どこからともなく現れ、たった１人でそれらを屠り、国を救ってしまった傳說の女精靈の通り名だ。

そんな存在に、艾麗婭は過去に救われていたようだ。
そして艾麗婭は、さらに言葉を續ける。

「わたしはその時に決めたんです。いつか、わたしも剣聖樣のように強くなって、困っている人々を助けてあげられるようになるんだって⋯⋯⋯その為にナイフの練習をして、修業の為に冒険者になったんです」

（ご主人⋯⋯！）

高潔な艾麗婭の生き方に。
塔瑪は心底、心をうたれた。

「ですが⋯⋯」

だが、そこまで言って。
艾麗婭は意氣消沈といった樣子で、顏を半分ほど沈めてしまう。

塔瑪は艾麗婭の言わんとしていることを察した。

たく桑の人を助けるどころか。
今日は、突然變異種とはいえ哥布林如きに後れをとり、塔瑪に助けられてしまった。
恐らくそれが悔しくてならないのだ。

（大丈夫だ、ご主人⋯⋯ご主人はまだ若い。その氣概さえあれば、これからどんどん強くなっていくであろう。それに我が輩もついている。ご主人が強くなるまで、決してその命を散らせはしない）

落ち込む艾麗婭の姿に。
塔瑪は改めて、この少女を守り拔くことを決意する。

そして、器用に泳ぎ。
艾麗婭の側まで近づくと、そのまま爪を立てぬように彼女の肩まで登り、彼女の頬に、自分の頭をスリスリと擦りつける。

「塔瑪⋯⋯⋯呋呋っ、君は優しいんですね」

そのおかげもあってか。
艾麗婭の顏に笑顏が戻る。

今宵⋯⋯１人の少女と一匹の貝希摩斯は、絆を深めるのだった。